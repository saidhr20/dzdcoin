<!DOCTYPE html>
<html lang="en">
<head>
<title>DZD Cash | Boutique</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="DZD CASH">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="dzdcash">
<meta name="keywords" content="dzdcash">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="styles/shop_styles.css">
<link rel="stylesheet" type="text/css" href="styles/shop_responsive.css">
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>

</head>

<body>

<div class="super_container">

	<!-- Header -->

    @include('header')

	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/shop.jpg"></div>
		<div class="home_overlay"></div>
		<div class="home_content d-flex flex-column align-items-center justify-content-center text-center">
			<h2 class="home_title">{{$categoryName}}</h2>
		</div>
	</div>

	<!-- Shop -->

	<div class="shop">
		<div class="container">
			<div class="row">

				<div class="col-lg-4">

					<!-- Shop Sidebar -->
					<div class="shop_sidebar">
						<div class="sidebar_section">
							<div class="sidebar_title"><i class="fas fa-angle-double-down"></i> Catégories</div>
							<ul class="sidebar_categories">
                            <li><a href="{{route('shop.index')}}"><i class="fas fa-angle-right"></i>Toutes les catégories</a></li>
                            @foreach($superCategories as $superCategory)
                                    <li><a href="{{route('shop.index',['superCategory' =>$superCategory->slugs])}}"><i class="fas fa-angle-right"></i> {{$superCategory->name}}</a>
                                    <ul>
                                        @foreach($superCategory->category()->get() as $category)
                                            <li class="category"><a href="{{route('shop.index',['category' =>$category->slugs])}}">{{$category->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
								@endforeach
							</ul>
						</div>

					</div>

				</div>

				<div class="col-lg-8">

					<!-- Shop Content -->

					<div class="shop_content">
						<div class="shop_bar clearfix">
							<div class="shop_product_count"><span>{{$product->count()}}</span> Articles trouvés</div>
							<div class="shop_sorting">
								<span>Trier par:</span>
								<ul>
									<li>
										<span class="sorting_text">Les mieux notés<i class="fas fa-chevron-down"></i></span>
										<ul>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Les mieux notés</li>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Nom</li>
											<li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>prix</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>

						<div class="product_grid">

							<!-- product Item -->
                            @forelse($product as $prod)
							<div class="product_item
                                @if(!is_null($prod->discount))  {{"discount"}}
                                @elseif($prod->feature)  {{"is_new"}} @endif

                                col-lg-3 col-sm-12">
								<div class="product_image d-flex flex-column align-items-center justify-content-center">
                                    <a href="{{route('shop.show',$prod->slug)}}"><img src={{ url ("images/product/$prod->id.png") }} alt=""></a></div>
								<div class="product_content">
									<div class="product_price">{{ number_format($prod->price, 0, ',', ' ')." ".$prod->market }}</div>
									<div class="product_name"><div>
                                            <a href="{{route('shop.show',$prod->slug)}}" tabindex="0">{{$prod->name}}</a></div></div>
								</div>
								<div class="product_fav"><i class="fas fa-heart"></i></div>
								<ul class="product_marks">
									<li class="product_mark product_discount">@if(!is_null($prod->discount))  {{"- ".$prod->discount."%"}} @endif</li>
									<li class="product_mark product_new">New</li>
								</ul>

							</div>
                                @empty
                                <div> Aucun Article trouvé ...</div>
                                @endforelse

						</div>

						<!-- Shop Page Navigation -->

						<div class="shop_page_nav d-flex flex-row">
							<div class="page_prev d-flex flex-column align-items-center justify-content-center"><i class="fas fa-chevron-left"></i></div>
							<ul class="page_nav d-flex flex-row">
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">...</a></li>
								<li><a href="#">21</a></li>
							</ul>
							<div class="page_next d-flex flex-column align-items-center justify-content-center"><i class="fas fa-chevron-right"></i></div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Recently Viewed -->

	<div class="viewed">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="viewed_title_container">
						<h3 class="viewed_title">Regardé récemment</h3>
						<div class="viewed_nav_container">
							<div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
							<div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
						</div>
					</div>

					<div class="viewed_slider_container">

						<!-- Recently Viewed Slider -->

						<div class="owl-carousel owl-theme viewed_slider">

							<!-- Recently Viewed Item -->
                            @foreach($recentViewed as $rs)
                                <div class="owl-item">
                                    <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="viewed_image">
                                            <a href="{{route('shop.show',$rs->slug)}}">
                                                <img src={{ url ("images/product/$rs->id.png")}} alt="">
                                            </a></div>
                                        <div class="viewed_content text-center">
                                            <div class="viewed_price">{{number_format($rs->price,0,',',' ')." ".$rs->market}}<span>{{$rs->last_price}}</span></div>
                                            <div class="viewed_name"><a href="#">{{$rs->name}}</a></div>
                                        </div>
                                        <ul class="item_marks">

                                            <li class="item_mark item_new">new</li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach

                        </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

    @include('footer')

	<!-- Copyright -->


</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/shop_custom.js"></script>
</body>

</html>
