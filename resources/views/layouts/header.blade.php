<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('header.name', 'DZD CASH | AUTHENTIFICATION') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href={{ URL::asset("styles/bootstrap4/bootstrap.min.css") }}>
    <link rel="stylesheet" href={{ URL::asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" ) }}>
    <link rel="stylesheet" href={{ URL::asset("styles/main_styles.css") }}>
    <link rel="stylesheet" href={{ URL::asset("styles/main_log.css") }}>
    <link rel="stylesheet" href={{ URL::asset("styles/responsive.css") }}>
</head>
<body>
<!-- Header -->
<header class="header" >
    <!-- Top Bar -->
    <div class="top_bar">
        <div class="container">
            <div class="row" >
                <div class="col d-flex flex-row">
                    <div class="top_bar_contact_item"><div class="top_bar_icon"><img src={{ url ("images/phone.png")}}  alt=""></div>0000000000</div>
                    <div class="top_bar_contact_item"><div class="top_bar_icon"><img src={{ url ("images/mail.png")}}  alt=""></div><a href="mailto:fastsales@gmail.com">admin@admin.com</a></div>
                    <div class="top_bar_content ml-auto">
                        <div class="top_bar_menu">
                            <ul class="standard_dropdown top_bar_dropdown">
                                <li>
                                    <a href="#">English<i class="fas fa-chevron-down"></i></a>
                                    <ul>
                                        <li><a href="#">Français</a></li>
                                        <li><a href="#">Arabe</a></li>
                                        <li><a href="#">English</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>


                        @guest
                            <a href="{{route('login')}}" type="button" class="btn btn-primary">Se connecter</a>
                            <a href="{{route('register')}}" type="button" class="btn btn-outline-primary">S'inscrire</a>
                        @else
                            <div class="user_icon"><img src={{ url ("images/user.svg")}}  alt=""></div>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                               type="button" class="btn btn-primary">Se déconnecter</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Main -->
    <div class="header_main" >
        <div class="container">
            <div class="row">

                <!-- Logo -->
                <div class="col-lg-2 col-sm-3 col-3 order-1" >
                    <div class="logo_container">
                        <div class="logo"><a href="{{route('main')}}"><img src={{ url ("images/logo2.png")}} ></a></div>
                    </div>
                </div>
                <div class="col-lg-1 col-sm-1 col-1 order-2">
                    <div class="separateur"></div>
                </div>
                <!-- Search -->
                <div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
                    <div class="header_search">
                        <div class="header_search_content">
                            <div class="header_search_form_container">
                                <form action="#" class="header_search_form clearfix" >
                                    <input type="search" required="required" class="header_search_input" placeholder="Search for products...">
                                    <div class="custom_dropdown">
                                        <div class="custom_dropdown_list">
                                            <span class="custom_dropdown_placeholder clc">Toutes les catégories</span>
                                            <i class="fas fa-chevron-down"></i>
                                            <ul class="custom_list clc">
                                                <li><a class="clc" href="{{route('shop.index')}}">La Boutique </a></li>
                                                <li><a class="clc" href="#">Promotions</a></li>
                                                <li><a class="clc" href="#">Témoignages</a></li>
                                                <li><a class="clc" href="{{route('guide')}}">Guide d'achat </a></li>
                                                <li><a class="clc" href="#">Services </a></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <button type="submit" class="header_search_button trans_300" value="Submit"><img src={{ url ("images/search.png")}}  alt=""></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Main Navigation -->

    <nav class="main_nav" style="width: 100%;padding: 0;margin: 0">
        <div class="container" >
            <div class="row" >
                <div class="col-lg-12" style=" width: 100%;padding: 0;margin: 0" >

                    <div class="main_nav_content d-flex flex-row" >

                        <!-- Categories Menu -->

                        <!-- Main Nav Menu -->

                        <div class="main_nav_menu ml-auto" >
                            <ul class="standard_dropdown main_nav_dropdown">
                                <li><a href="{{route('main')}}">Accueil <i class="fas fa-chevron-down"></i></a></li>
                                <li class="hassubs">
                                    <a href="{{route('shop.index')}}">Boutique<i class="fas fa-chevron-down"></i></a>
                                    <ul>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'solde'])}}">Solde</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'abonnement'])}}">Abonnement</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'rechargement'])}}">Rechargement</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'videos-games'])}}">Video games and credits</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'serveurs-tv'])}}">Serveurs et IpTv</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'services-electronique'])}}">Services Electronique</a></li>
                                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'accessories'])}}">Accessories</a></li>
                                    </ul>
                                </li>
                                <li class="hassubs">
                                    <a href="{{route('guide')}}">Guide<i class="fas fa-chevron-down"></i></a>
                                    <ul>
                                        <li><a href="{{route('guide')}}">Comment faire une commande ?<i class="fas fa-chevron-right"></i></a></li>
                                        <li><a href="#">Comment acheter un produit ?<i class="fas fa-chevron-right"></i></a></li>
                                        <li><a href="{{route('guide')}}">Comment envoyer un reçu de payement ?<i class="fas fa-chevron-right"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="../blog.html">Qui Sommes Nous ?<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="../blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="{{route('contact.index')}}">Contact<i class="fas fa-chevron-down"></i></a></li>
                            </ul>
                        </div>

                        <!-- Menu Trigger -->

                        <div class="menu_trigger_container ml-auto">
                            <div class="menu_trigger d-flex flex-row align-items-center justify-content-end" >
                                <div class="menu_burger" >
                                    <div class="menu_trigger_text">Menu</div>
                                    <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </nav>

    <!-- Menu -->

    <div class="page_menu">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page_menu_content">

                        <div class="page_menu_search">
                            <form action="#">
                                <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
                            </form>
                        </div>
                        <ul class="page_menu_nav">
                            <li class="page_menu_item has-children">
                                <a href="#">Language<i class="fa fa-angle-down"></i></a>
                                <ul class="page_menu_selection">
                                    <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="#">Français<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="#">Arabe<i class="fa fa-angle-down"></i></a></li>
                                </ul>
                            </li>
                            <li class="page_menu_item has-children">
                                <a href="#">Panier<i class="fa fa-angle-down"></i></a>
                                <ul class="page_menu_selection">
                                    <li><a href="{{route('cart.index')}}">DZD Dinnar Algerien<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="{{route('carte.create')}}">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                                </ul>
                            </li>

                            <li class="page_menu_item"><a href="{{route('shop.index')}}">Boutique<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="../blog.html">blog<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="{{route('contact.index')}}">contact<i class="fa fa-angle-down"></i></a></li>
                            @guest
                                <li class="page_menu_item"><a href="{{route('register')}}">S'inscrire</a></li>
                                <li class="page_menu_item"><a href="{{route('login')}}">Se connecter</a></li>
                            @else
                                <li class="page_menu_item"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                                              type="button" class="btn btn-primary">Se déconnecter</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form></li>
                            @endguest
                        </ul>
                        <div class="menu_contact">
                            <div class="menu_contact_item"><div class="menu_contact_icon"><img src={{ url ("images/phone_white.png")}}  alt=""></div>0654545445</div>
                            <div class="menu_contact_item"><div class="menu_contact_icon"><img src={{ url ("images/mail_white.png")}}  alt=""></div><a href="mailto:fastsales@gmail.com">test00@gmail.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')


        </div>

    </div>
    @include('footer')
</header>


<script src={{ URL::asset("js/jquery-3.3.1.min.js") }}></script>
<script src={{ URL::asset("styles/bootstrap4/popper.js") }}></script>
<script src={{ URL::asset("styles/bootstrap4/bootstrap.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/TweenMax.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/TimelineMax.min.js") }}></script>
<script src={{ URL::asset("plugins/scrollmagic/ScrollMagic.min.js") }}> </script>
<script src={{ URL::asset("plugins/greensock/animation.gsap.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/ScrollToPlugin.min.js") }}></script>
<script src={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") }}></script>
<script src={{ URL::asset("plugins/slick-1.8.0/slick.js") }}></script>
<script src={{ URL::asset("plugins/easing/easing.js") }}></script>
<script src={{ URL::asset("js/custom.js") }}></script>



</body>
