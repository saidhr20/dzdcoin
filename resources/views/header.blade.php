<!-- Header -->
    <header class="header">
        <!-- Top Bar -->
        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 d-flex flex-row">
                        <div class="top_bar_contact_item"><div class="top_bar_icon"><img src={{ url ("images/phone.png")}}  alt=""></div>0000000000</div>
                        <div class="top_bar_contact_item"><div class="top_bar_icon"><img src={{ url ("images/mail.png")}}  alt=""></div><a href="mailto:fastsales@gmail.com">admin@admin.com</a></div>
                    </div>
                    @guest

                        <div class="col-sm-6 text-right">
                        <div class="top_bar_menu">
                                    <ul class="standard_dropdown top_bar_dropdown">
                                        <li>
                                            <a href="#">English<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li><a href="#">Français</a></li>
                                                <li><a href="#">Arabe</a></li>
                                                <li><a href="#">English</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                        </div>
                        <a href="{{route('login')}}" type="button" class="btn btn-primary">Se connecter</a>
                        <a href="{{route('register')}}" type="button" class="btn btn-outline-primary">S'inscrire</a>
                        </div>
                    @else
                        <div class="col-sm-3 text-right">
                            <div class="top_bar_menu">
                                <ul class="standard_dropdown top_bar_dropdown">
                                    <li>
                                        <a href="#">English<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li><a href="#">Français</a></li>
                                            <li><a href="#">Arabe</a></li>
                                            <li><a href="#">English</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 d-flex flex-row">
                            <div class="container">
                                <div class="half">
                                    <label for="profile2" class="profile-dropdown dropdown">
                                            <input type="checkbox" id="profile2">
                                            <img src="https://cdn0.iconfinder.com/data/icons/avatars-3/512/avatar_hipster_guy-512.png">
                                            <span>
                                                {{ Auth::user()->name }}
                                            </span>
                                            <label class="bars" for="profile2"><i class="fas fa-bars"></i></label>
                                            <ul>
                                                <li><a href="#"><i class="fas fa-user"></i>Profile</a></li>
                                                <li><a href="#"><i class="fas fa-shopping-cart"></i>Commandes</a></li>
                                                <li><a href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a></li>
                                            </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endguest
                    </div>


                </div>
            </div>

        <!-- Header Main -->
        <div class="header_main">
            <div class="container">
                <div class="row">

                    <!-- Logo -->
                    <div class="col-lg-2 col-3 order-1">
                        <div class="logo_container">
                            <div class="logo"><a href="{{route('main')}}"><img src={{ url ("images/logos.png")}} ></a></div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-0 order-lg-1"></div>
                    <!-- Search -->
                    <div class="col-lg-6 col-12 order-lg-2 order-3  justify-content-lg-end text-right ">
                        <div class="header_search">
                            <div class="header_search_content">
                                <div class="header_search_form_container">
                                    <form action="#" class="header_search_form clearfix">
                                        <input type="search" required="required" class="header_search_input" placeholder="Search for products...">
                                        <div class="custom_dropdown">
                                            <div class="custom_dropdown_list">
                                                <span class="custom_dropdown_placeholder clc">Catégories</span>
                                                <i class="fas fa-chevron-down"></i>
                                                <ul class="custom_list clc">
                                                    <li><a class="clc" href="{{route('shop.index')}}">Boutique</a></li>
                                                    <li><a class="clc" href="#">Produits</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                        <button type="submit" class="header_search_button trans_300" value="Submit"><img src={{ url ("images/search.png")}}  alt=""></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Wishlist -->
                    <div class="col-lg-3 col-9 order-lg-3 order-1 text-lg-right justify-content-end">
                        <div class="wishlist_cart d-flex flex-row align-items-center justify-content-center">
                            <!-- Cart -->
                            <div class="cart">
                                <div class="cart_container d-flex flex-row align-items-center justify-content-end">
                                    <div class="cart_icon">
                                        <a href="{{route('cart.index')}}"><img src={{ url ("images/cart.png")}}  alt="panier"></a>
                                        <a href="{{route('cart.index')}}"><div class="cart_count"><span>
											{{Cart::instance('DZD_CART')->count()}}</span></div></a>
                                    </div>
                                    <div class="cart_content">
                                        <a href="{{route('cart.index')}}"><div class="cart_text">TOTAL</div></a>
                                        <a href="{{route('cart.index')}}"><div class="cart_price">{{Cart::instance('DZD_CART')->subtotal(0)}} DZD </div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

        <!-- Main Navigation -->

        <nav class="main_nav">
            <div class="container">
                <div class="row">
                    <div class="col">

                        <div class="main_nav_content d-flex flex-row">

                            <!-- Categories Menu -->

                            <div class="cat_menu_container">
                                <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                                    <div class="cat_burger"><span></span><span></span><span></span></div>
                                    <div class="cat_menu_text">catégories</div>
                                </div>

                                <ul class="cat_menu">
                                    @foreach(\App\SuperCategory::all() as $supercategory)
                                        <li class="hassubs"><a href="">{{$supercategory->name}}<i class="fas fa-chevron-right"></i></a>
                                        <ul>
                                            @foreach($supercategory->category()->get() as $category)
                                                <li><a href="{{route('shop.index',['category' =>$category->slugs])}}">{{$category->name}}<i class="fas fa-chevron-right"></i></a></li>
                                            @endforeach
                                        </ul>
                                    @endforeach
                                        </li>
                                </ul>
                            </div>

                            <!-- Main Nav Menu -->

                            <div class="main_nav_menu ml-auto ">
                                <ul class="standard_dropdown main_nav_dropdown">
                                    <li><a href="{{route('main')}}">Accueil <i class="fas fa-chevron-down"></i></a></li>
                                    <li class="hassubs">
                                        <a href="#">A propos</a>
                                    </li>
                                    <li class="hassubs">
                                        <a href="{{route('shop.index')}}">Boutique</a>
                                    </li>
                                    <li class="hassubs">
                                        <a href="{{route('guide')}}">Guide d'achat<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li><a href="{{route('guide')}}">Comment faire une commande ?<i class="fas fa-chevron-right"></i></a></li>
                                            <li><a href="{{route('guide')}}">Comment envoyer le reçu de paiement ?<i class="fas fa-chevron-right"></i></a></li>
                                            <li><a href="{{route('guide')}}">Comment recevoir votre produit ?<i class="fas fa-chevron-right"></i></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{route('contact.index')}}">Contactez Nous<i class="fas fa-chevron-down"></i></a></li>
                                </ul>
                            </div>

                            <!-- Menu Trigger -->

                            <div class="menu_trigger_container ml-auto">
                                <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                                    <div class="menu_burger" >
                                        <div class="menu_trigger_text">Menu</div>
                                        <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <!-- Menu -->

        <div class="page_menu">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="page_menu_content">
                            <div class="page_menu_search">
                                <form action="#">
                                    <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
                                </form>
                            </div>
                            <ul class="page_menu_nav">
                                <li class="page_menu_item has-children">
                                    <a href="#">Language<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Français<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Arabe<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item has-children">
                                    <a href="#">Panier<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="{{route('cart.index')}}">DZD Dinnar Algerien<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="{{route('carte.create')}}">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>

                                <li class="page_menu_item"><a href="{{route('shop.index')}}">Boutique<i class="fa fa-angle-down"></i></a></li>
                                <li class="page_menu_item"><a href="{{route('contact.index')}}">contact<i class="fa fa-angle-down"></i></a></li>
                                @guest
                                    <li class="page_menu_item"><a href="{{route('register')}}">S'inscrire</a></li>
                                    <li class="page_menu_item"><a href="{{route('login')}}">Se connecter</a></li>
                                @else
                                    <li class="page_menu_item"><a href=""> {{ Auth::user()->name }}</a></li>
                                    <li class="page_menu_item"><a href="{{route('logout')}}">Se déconnecter</a></li>
                                @endguest
                            </ul>
                            <div class="menu_contact">
                                <div class="menu_contact_item"><div class="menu_contact_icon"><img src={{ url ("images/phone_white.png")}}  alt=""></div>0654545445</div>
                                <div class="menu_contact_item"><div class="menu_contact_icon"><img src={{ url ("images/mail_white.png")}}  alt=""></div><a href="mailto:fastsales@gmail.com">test00@gmail.com</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>


