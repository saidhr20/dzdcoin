<!DOCTYPE html>
<html lang="en">
<head>
    <title>DZD Cash | Guide d'achat</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="stylesheet" type="text/css" href={!! asset("styles/bootstrap4/bootstrap.min.css") !!}>
    <link href={!! asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css") !!} rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/animate.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_styles.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_responsive.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>


</head>

<body>

<div class="super_container">

    <!-- Header -->
    @include('header')

    <div class="guide justify-content-center">
        <div class="guide-title">
            <h1>Guide d'achat</h1>
        </div>
        <div class="col-lg-12 col-12" id="top">
                                    <div class="wizard-progress">
                                        <div class="step current complete" id="step-1">
                                            <button class="carre" id="node-1">1</button>
                                            Passer une commande
                                        </div>
                                        <div class="step current" id="step-2">
                                            <button class="carre" id="node-2">2</button>
                                            Passer au paiement
                                        </div>
                                        <div class="step" id="step-3">
                                            <button class="carre" id="node-3">3</button>
                                            Recevoir votre produit
                                        </div>
                                </div>
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                            <div class="etap col-lg-8" id="etap-1">
                                                    <div class="text-md-left">
                                                        <h2>Comment passer une commande ?</h2>
                                                        <p>Pour passer une commande veuillez suivre les étapes suivantes :</p>
                                                        <ul>
                                                            <li>Choisissez la catégorie qui vous intéresse<br/></li>
                                                            <div style="list-style: none"><img src="{{ url ("images/guide/etape1.png")}}" class="img-fluid"></div>
                                                            <li>Sélectionnez le produit que vous souhaitez acheter <br/></li>
                                                            <div style="list-style: none"><img src="{{ url ("images/guide/etape2.png")}}" class="img-fluid"></div>
                                                            <li>Choisissez la quantité que vous voulez acheter et cliquez sur ‘‘ajouter au panier ’’ <br/></li>
                                                            <div style="list-style: none"><img src="{{ url ("images/guide/etape3.png")}}" class="img-fluid"></div>
                                                            <li>Vérifiez sur votre panier la quantité, le prix unitaire,  et le prix total, puis cliquez sur "procédure et paiement "<br/></li>
                                                            <div style="list-style: none"><img src="{{ url ("images/guide/etape4.png")}}" class="img-fluid"></div>
                                                            <li>Remplissez le formulaire qui s’affichera, et cliquez sur ‘Commander’ puis passez a l’étape suivante<br/></li>
                                                            <div style="list-style: none"><img src="{{ url ("images/guide/etape5.png")}}" class="img-fluid"></div>
                                                        </ul>
                                                    </div>
                                                    <div class="passer text-md-center col-lg-6 col-sm-2">
                                                        <button type="btn" class="btn btn-primary btn-group-lg" id="suivant-1">Passer au paiement <i class="fas fa-chevron-circle-right"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                    </div>
                                        <div class="row" >
                                             <div class="col-lg-2"></div>
                                                <div class="etap col-lg-8" id="etap-2" style="display: none">
                                                    <div class="text-md-left" >
                                                       <h2>Comment effectuer les paiements ?</h2>
                                                        <p>À la fin de votre commande un email de
                                                            confirmation vous sera envoyé : </p>
                                                        <ul>
                                                         <li>Vérifiez vos informations ensuite confirmez votre commande.</li>
                                                         <li>Effectuez votre paiement par la méthode de paiement que vous avez choisi (vous devez
                                                         envoyer le montant exact) .</li>
                                                         <li>Veuillez envoyer une photo de reçu a l'adresse mail suivante XXXXXXX contenant
                                                             le code de votre commande ainsi que le nom du site dzdcash.</li>
                                                     </ul>
                                                    </div>
                                                    <div class="row">
                                                    <div class="passer text-md-center col-lg-6">
                                                            <button type="btn" class="btn btn-primary" id="precedent-1">
                                                                <i class="fas fa-chevron-circle-left"></i> Passer une commande
                                                            </button>
                                                    </div>
                                                    <div class="passer text-md-center col-lg-6">
                                                        <button type="btn" class="btn btn-primary" id="suivant-2">
                                                            Recevoir votre produit <i class="fas fa-chevron-circle-right"></i>
                                                        </button>
                                                    </div>
                                                    </div>
                                                </div>
                                        </div>

                                     <div class="row" >
                                            <div class="col-lg-2"></div>
                                                    <div class="etap col-lg-8" id="etap-3" style="display: none">
                                                    <div class="text-md-left" >
                                                        <h2>Comment recevoir votre produit ?</h2>
                                                        <p>Après avoir effectué les étapes précédentes,
                                                            nous procédons à une vérification du paiement.
                                                            À l'issue de cette derrière vous recevrez
                                                            un email de validation de paiement et vous pourrez par la suite récupérer votre produit.</p>
                                                    </div>
                                                    <div class="col-lg-4 img-shipped">
                                                        <img src="{{ url ("images/shipped.png")}}" alt="" class="img-fluid">
                                                    </div>
                                                        <div class="row">
                                                            <div class="passer text-md-center col-lg-6">
                                                                 <button type="btn" class="btn btn-primary" id="precedent-2">
                                                                    <i class="fas fa-chevron-circle-left"></i>  Passer au paiement
                                                                </button>
                                                             </div>
                                                            <div class="passer text-md-center col-lg-6">
                                                            <button type="btn" class="btn btn-primary" onclick="window.location.href = '{{route('shop.index')}}';">
                                                                boutique <i class="fas fa-shopping-cart"></i>
                                                            </button>
                                                            </div>
                                                        </div>
                                                </div>
                                     </div>
                                            </div>
                            </div>
</div>
    <div>
    </div>

@include('footer')


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script !src="">
    $("#node-1,#precedent-1").on( "click", function () {
        $("#etap-1").css("display","block");
        $("#etap-2").css("display","none");
        $("#etap-3").css("display","none");
        $("#step-1").addClass('current');
        $("#step-2").removeClass('complete');
        $("#step-2").addClass('current');
        $("#step-3").removeClass('complete current');
        $("html, body").animate({
            scrollTop: $("#top").offset().top
        });
    } );
    $("#node-2,#suivant-1,#precedent-2").on( "click", function () {
        $("#etap-2").css("display","block");
        $("#etap-1").css("display","none");
        $("#etap-3").css("display","none");
        $("#step-2").addClass('current');
        $("#step-3").removeClass('complete current');
        $("#step-1").removeClass('current');
        $("#step-1").addClass('complete');
        $("html, body").animate({
            scrollTop: $("#top").offset().top
        });
    } );
    $("#node-3,#suivant-2").on( "click", function () {
        $("#etap-3").css("display","block");
        $("#etap-1").css("display","none");
        $("#etap-2").css("display","none");
        $("#step-2").addClass('complete');
        $("#step-1").addClass('complete');
        $("#step-3").addClass('complete current');
        $("#step-2,#step-1").removeClass('current');
        $("html, div").animate({
            scrollTop: $("#top").offset().top
        });
    } );

</script>
<script src={!! asset("js/jquery-3.3.1.min.js") !!}></script>
<script src={!! asset("styles/bootstrap4/popper.js") !!}></script>
<script src={!! asset("styles/bootstrap4/bootstrap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TweenMax.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TimelineMax.min.js") !!}></script>
<script src={!! asset("plugins/scrollmagic/ScrollMagic.min.js") !!}></script>
<script src={!! asset("plugins/greensock/animation.gsap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/ScrollToPlugin.min.js") !!}></script>
<script src={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") !!}></script>
<script src={!! asset("plugins/easing/easing.js") !!}></script>
<script src={!! asset("js/product_custom.js") !!}></script>
</body>
</html>

