<!DOCTYPE html>
<html lang="en">
<head>
    <title>DZD Cash | Confirmation</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="stylesheet" href={!! asset("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css") !!} integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"  crossorigin="anonymous">
    <link href={!! asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css") !!} rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/animate.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_styles.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_responsive.css") !!} >
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>


</head>

<body>

<div class="super_container">
    @include('header')
        @if(session()->has('error_message'))
            <div class="py-4 text-center">
            <div class="alert alert-danger">{{session()->get('error_message')}}</div>
            </div>
        @else
        <div class="py-4 text-center">
            <div class="site-header">
                <h1 class="site-header__title" data-lead-id="site-header-title">Merci !</h1>
            </div>
            <div class="main-content">
                <div class="checkmark">
                    <svg version="1.1"  x="0px" y="0px"
                         viewBox="0, 0, 100, 100" id="checkmark">
        <g transform="">
            <circle class="path" fill="none"  stroke-width="4" stroke-miterlimit="10" cx="50" cy="50" r="44"/>
            <circle class="fill" fill="none" stroke-width="4" stroke-miterlimit="10" cx="50" cy="50" r="44"/>
            <polyline class="check" fill="none" stroke="#28a745" stroke-width="8" stroke-linecap="round" stroke-miterlimit="10"
                      points="70,35 45,65 30,52  "/>
        </g>
    </svg>


                </div>
            </div>
            <div class="num_commande">
                COMMANDE N° <span>{{$order->id}}</span>
            </div>
        </div>
        <div class="container-lg row justify-content-center mb-lg-5">
            @if(session()->has('success_message'))
                <div class="alert alert-success">{{session()->get('success_message')}}</div>
            @endif
            <div class="col-lg-4 order-lg-2 order-sm-1">
                <div class="container-perso">
                    <div class="d-flex justify-content-between align-items-center panier_title">
                        <span class="deals_item_name">Bon d'achat</span>
                        <span class="badge badge-danger badge-pill">{{$cart->count()}} Article </span>
                    </div>
                    <div class="deals_item">
                        <ul class="list-group">
                            @foreach($cart as $item)
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon"><img src={{url("images/product/$item->id.png")}} alt="" style="width:75px;">
                                            <span class="deals_timer_title">{{$item->name}} </span>
                                        </div>
                                        <span class="badge badge-warning quantity_e"><small>{{$item->pivot->quantity." ".$item->etq." x ".$item->price." ".$item->market}}</small></span>
                                    </div>
                                    <span class="bestsellers_price discount">{{$item->price*$item->pivot->quantity." ".$item->market}}</span>
                                </li>
                            @endforeach
                            <li class="list-group-item d-flex justify-content-between ">
                                <span class="dealer_total">Total : </span>
                                <span class="total_price"><strong>{{$order->subtotal." ".$item->market}}</strong></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="container-perso">
                    <div class="d-flex justify-content-between align-items-center panier_title">
                        <span class="deals_item_name">Mode de paiement :</span>
                        <span class="badge badge-success badge-pill">{{$order->paymentMethod}}</span>
                    </div>
                    <div class="deals_item">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-user"></i>
                                        <span class="deals_timer_title">Name & last name :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->first_name." ".$order->last_name}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-envelope"></i>
                                        <span class="deals_timer_title">Adresse Email :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->email}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-phone-square"></i>
                                        <span class="deals_timer_title">Phone number:</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->phone}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fa fa-id-card" aria-hidden="true"></i>
                                        <span class="deals_timer_title">Payment method</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->paymentMethod}}</span>
                            </li>

                        </ul>
                    </div>

                </div>
                    <div class="container-perso">
                        <div class="d-flex justify-content-between align-items-center panier_title">
                            <span class="deals_item_name">Vos informations</span>
                        </div>
                    <div class="deals_item">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-user"></i>
                                        <span class="deals_timer_title">Name & last name :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->first_name." ".$order->last_name}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-envelope"></i>
                                        <span class="deals_timer_title">Adresse Email :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->email}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-phone-square"></i>
                                        <span class="deals_timer_title">Phone number:</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->phone}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fa fa-id-card" aria-hidden="true"></i>
                                        <span class="deals_timer_title">Payment method</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$order->paymentMethod}}</span>
                            </li>

                        </ul>
                    </div>
                    </div>
            </div>
            <div class="col-md-7 order-1">
                <div class="container-info">
                    <div class="container-perso">
                        <div class="d-flex justify-content-between align-items-center panier_title">
                            <span class="deals_item_name">Etapes suivante</span>
                        </div>
                        <div class="deals_item">
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between lh-condensed" >
                                    <div>
                                        <div class="etapes_title">Commande effectuer</div>
                                        <div class="char_icon"><p>
                                            Félicitations! Votre commande a été bien reçu .
                                            Une confirmation vous sera envoyée par mail.
                                            Vous recevrez un SMS ou un appel de la part de notre Service Client,
                                                veuillez consulter votre boite mail pour suiver les instructions du paiement
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon">
                                            <div class="etapes_title">Confirmer votre commande</div>
                                            <p>
                                           Le paiement se fait par la methode que vous avez choisi
                                                par exemple : "versement CCP (Algérie Poste)".
                                                Vous recevrez les coordonnées de paiement dès que vous passez votre commande.
                                                ensuite , vous devez verser le montant exacte exactement comment s'ecris sur votre bon du commande .
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon">
                                            <div class="etapes_title">Effectuer vos paiement</div>
                                        <p>
                                            Aprés d'avoir faire le paiement , vous devez ecrire le numero de votre commande plus
                                                "dzdcoin.com" sur votre reçu du paiement et l'envoyer sur notre Email pour sur cette page.
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon">
                                            <div class="etapes_title">Recevoir de votre produit</div>
                                            <p>
                                                Nous traiterons ensuite votre commande et vous recevrez votre produit
                                                par email dans une periode de moins de 12h .
                                            </p>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                        <div class="container-perso">
                            <div class="d-flex justify-content-between align-items-center panier_title">
                                <span class="deals_item_name">Suivre votre commande</span>
                            </div>
                            <div class="wizard-progress col-sm-12">
                                <div class="step complete">
                                    <div class="node"></div>Commande passée</div>
                                <div class="step current">
                                    <div class="node"></div>Commande confirmée</div>
                                <div class="step ">
                                    <div class="node"></div>Paiement effectué</div>
                                <div class="step">
                                    <div class="node"></div>Produit livrée</div>
                            </div>
                        <div class="deals_item">
                            <div class="text-center py-4 col-lg-12  justify-content-center">
                                    <p>Connecter sur votre compte pour que vous puissez suivre votre commande</p>
                                    <button type="btn" class="btn btn-info btn-group-lg ">
                                        {{ __('VOIR VOS COMMANDES') }}
                                    </button>
                                @guest
                                    <div class="char_icon text-md-center">
                                        <div class="col-form-label">
                                            {{ __('Vous possédez déjà un compte ?') }}
                                        </div>
                                        <div class="col-form-label">
                                            <a href="{{route('login')}}">{{ __('CONNECTEZ-VOUS') }}</a>
                                        </div>
                                    </div>
                                @endguest
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endif


<div>

</div>

    @include('footer')


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script !src="">
    $(document).ready(function () {

        $(".check").attr("class", "check check-complete success");
        $(".fill").attr("class", "fill fill-complete success");
        $(".path").attr("class", "path path-complete");

    });


</script>
<script src={!! asset("https://code.jquery.com/jquery-3.3.1.slim.min.js") !!} integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
<script src={!! asset("js/popper.min.js") !!}></script>
<script src={!! asset("js/bootstrap.min.js") !!}></script>
<script src={!! asset("js/holder.min.js") !!}></script>
<script src={!! asset("js/jquery-3.3.1.min.js") !!}></script>
<script src={!! asset("styles/bootstrap4/popper.js") !!}></script>
<script src={!! asset("styles/bootstrap4/bootstrap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TweenMax.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TimelineMax.min.js") !!}></script>
<script src={!! asset("plugins/scrollmagic/ScrollMagic.min.js") !!}></script>
<script src={!! asset("plugins/greensock/animation.gsap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/ScrollToPlugin.min.js") !!}></script>
<script src={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") !!}></script>
<script src={!! asset("plugins/easing/easing.js") !!}></script>
<script src={!! asset("js/product_custom.js") !!}></script>
</body>
</html>

