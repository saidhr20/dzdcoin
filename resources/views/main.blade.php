<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>DZD Cash | Home</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/bootstrap4/bootstrap.min.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" ) }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/animate.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/slick-1.8.0/slick.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/main_styles.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/responsive.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/header.css") }}>
</head>
<body>
<div class="super_container">
@include('header')

<!--Promotion banner -->
<div class="banner_2">
    <div class="banner_2_container">
        <div class="banner_2_dots"></div>
        <div class="owl-carousel owl-theme banner_2_slider">
            <div class="owl-item">
                <!--Promotion-->
                <?php

                foreach($products as  $ann){
                if(!is_null($ann->color)){
                ?>
                <div class="banner">
                    <div class="banner_background" style="background-image:url({{ url ("images/banner_background.jpg")}});"></div>
                    <div class="container fill_height">
                        <div class="row fill_height">
                            <div class="banner_product_image"><img alt="" src= {{ url ("images/$ann->id.png")}} ></div>
                            <div class="col-lg-4 offset-lg-4 fill_height">
                                <div class="banner_content">
                                    <h1 class="banner_text" style="color:{{$ann->color}};">{{$ann->name." Promotion" }}</h1>
                                    <div class="banner_price"><span>{{$ann->last_price." ".$ann->market}} </span>{{number_format($ann->price,0,',',' ')." ".$ann->market}} </div>
                                    <div class="banner_product_name">{{  $ann->sub_name}} </div>
                                    <div class="button banner_button" style="background-color:{{$ann->color}} "><a href="" > Acheter maintenant</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php  }}   ?>
        </div>
    </div>
</div>
<!--promotion de semaine-->
<div class="characteristics">
    <div class="container">
        <div class="row">
            <!-- Deals of the week -->
            <div class="deals_featured">
                <div class="container">
                    <div class="row">
                        <div class="col d-flex flex-lg-row flex-column align-items-center justify-content-start">
                            <!-- Deals -->
                            <div class="deals">
                                <div class="deals_title">Promo de semaine </div>
                                <div class="deals_slider_container">
                                    <!-- Deals Slider -->
                                    <div class="owl-carousel owl-theme deals_slider">
                                    <?php
                                            foreach($products as  $promo){
                                             if(!is_null($promo->discount)){
                                     ?>
                                    <!-- Deals Item -->
                                        <div class="owl-item deals_item">
                                            <div class="deals_image"><img src= {{ url ("images/promo/$promo->id.jpg")}} alt=""></div>
                                            <div class="deals_content">
                                                <div class="deals_info_line d-flex flex-row justify-content-start">
                                                    <div class="deals_item_category"><a href="#">{{$promo->name." ".$promo->class }}  Promotion</a></div>
                                                    <div class="deals_item_price_a ml-auto">{{$promo->last_price." ".$promo->market }} </div></div>
                                                <div class="deals_info_line d-flex flex-row justify-content-start">
                                                    <div class="deals_item_name">{{ $promo->name }}</div>
                                                    <div class="deals_item_price ml-auto">{{  $promo->price." ".$promo->market }} </div></div>
                                                <div class="available">
                                                    <div class="available_line d-flex flex-row justify-content-start">
                                                        <div class="available_title">Disponible: <span>{{ $promo->stock-$promo->sold." ".$promo->etq }}</span></div>
                                                        <div class="sold_title ml-auto">Déja vendu: <span>{{$promo->sold." ".$promo->etq }} </span></div></div>
                                                    <div class="available_bar"><span style="width:@if($promo->stock>0){{  ($promo->sold*100)/$promo->stock."%" }}@endif"></span></div>
                                                </div>
                                                <div class="deals_timer d-flex flex-row align-items-center justify-content-start">
                                                    <div class="deals_timer_title_container">
                                                        <div class="deals_timer_title">L'offre </div>
                                                        <div class="deals_timer_subtitle">se termine dans:</div>
                                                    </div>
                                                    <div class="deals_timer_content ml-auto">
                                                        <div class="deals_timer_box clearfix"
                                                             data-target-time="<?php
                                                             $date=date_create("$promo->date");
                                                             echo date_format($date,"M d,  Y"); ?>">
                                                            <div class="deals_timer_unit">
                                                                <div id="deals_timer1_day" class="deals_timer_day"></div>
                                                                <span>Jours</span>
                                                            </div>
                                                            <div class="deals_timer_unit">
                                                                <div id="deals_timer1_hr" class="deals_timer_hr"></div>
                                                                <span>Heures</span>
                                                            </div>
                                                            <div class="deals_timer_unit">
                                                                <div id="deals_timer1_min" class="deals_timer_min"></div>
                                                                <span>Mins</span>
                                                            </div>
                                                            <div class="deals_timer_unit">
                                                                <div id="deals_timer1_sec" class="deals_timer_sec"></div>
                                                                <span>Secs</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  }}  ?>
                                    </div>
                                </div>

                                <div class="deals_slider_nav_container">
                                    <div class="deals_slider_prev deals_slider_nav"><i class="fas fa-chevron-left ml-auto"></i></div>
                                    <div class="deals_slider_next deals_slider_nav"><i class="fas fa-chevron-right ml-auto"></i></div>
                                </div>
                            </div>

                            <!-- Featured -->
                            <div class="featured">
                                <div class="tabbed_container">
                                        <div class="tabs">
                                                <ul class="clearfix">
                                                    <li class="active">Feature</li>
                                                </ul>
                                                <div class="tabs_line"><span></span></div>
                                        </div>
                                    <!-- product Panel -->
                  <div class="product_panel panel active">
                        <div class="featured_slider slider">
                        @foreach($products as  $product)
                            @if($product->feature)
                                <!-- Slider Item -->
                                    <div class="featured_slider_item">
                                        <div class="border_active"></div>
                                        <div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                            <div class="product-class">
                                                <div class="product_image d-flex flex-column align-items-center justify-content-center">
                                                    <a href="{{route('shop.show',$product->slug)}}" class="image_product_featured"><img src= {{ url ("images/product/$product->id.png")}} alt=""></a></div>
                                                <div class="product_content">
                                                    <div class="product_price discount">
                                                        {{ number_format($product->price, 0, ',', ' ')." ".$product->market }}
                                                        <span><?php  if($product->stock>0) { echo '<span class="badge badge-secondary">'.$product->stock." ".$product->etq.'</span>'; }else{
                                                            echo '<span class="badge badge-danger">Rupture</span>';}  ?> </span></div>
                                                    <div class="product_name"><a href="{{route('shop.show',$product->slug)}}" >{{  $product->name  }} </a></div></div>
                                                    </div>
                                            <div class="product_extras">
                                                <form action="{{ route('cart.store', $product) }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    {!! Form::submit('Add to card',['class'=>'product_cart_button']) !!}
                                                </form>

                                            </div>
                                        </div>
                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                        <ul class="product_marks">
                                            <li class="product_mark product_discount">{{$product->discount }} % </li>
											<li class="product_mark product_new">new</li>
                                        </ul>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                  </div>






                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- Popular Categories -->
<div class="characteristics">
    <div class="container">
        <div class="row">
<!-- Char. Item -->
<div class="col-lg-3 col-md-6 char_col">

    <div class="char_item d-flex flex-row align-items-center justify-content-start">
        <div class="char_icon"><img src= {{ url ("images/char_1.png")}} alt=""></div>
        <div class="char_content">
            <div class="char_title">Free Sheeping </div>
            <div class="char_subtitle">No fees no delay</div>
        </div>
    </div>
</div>

<!-- Char. Item -->
<div class="col-lg-3 col-md-6 char_col">

    <div class="char_item d-flex flex-row align-items-center justify-content-start">
        <div class="char_icon"><img src= {{ url ("images/char_2.png")}} alt=""></div>
        <div class="char_content">
            <div class="char_title">Speed transfer</div>
            <div class="char_subtitle">From 5 min up to 12 Hours </div>
        </div>
    </div>
</div>

<!-- Char. Item -->
<div class="col-lg-3 -md-6 char_col">

    <div class="char_item d-flex flex-row align-items-center justify-content-start">
        <div class="char_icon"><img src= {{ url ("images/char_3.png")}} alt=""></div>
        <div class="char_content">
            <div class="char_title">Payement methods</div>
            <div class="char_subtitle">CCP & Baridi Mob & P2P</div>
        </div>
    </div>
</div>

<!-- Char. Item -->
<div class="col-lg-3 col-md-6 char_col">

    <div class="char_item d-flex flex-row align-items-center justify-content-start">
        <div class="char_icon"><img src= {{ url ("images/char_4.png")}} alt=""></div>
        <div class="char_content">
            <div class="char_title">Support contact</div>
            <div class="char_subtitle">12h/5 working days</div>
        </div>
    </div>
</div>
</div>
</div>
</div>



<!-- Best Sellers -->

<div class="best_sellers">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="tabbed_container">
                    <div class="tabs clearfix tabs-right">
                        <div class="new_arrivals_title">Hot Best Sellers</div>
                        <ul class="clearfix">
                            <li class="active">Top 20</li>
                            <li>Solde</li>
                            <li>Rechagement </li>
                        </ul>
                        <div class="tabs_line"><span></span></div>
                    </div>

                    <div class="bestsellers_panel panel active">

                        <!-- Best Sellers Slider -->

                        <div class="bestsellers_slider slider">

                            <!-- Best Sellers Item -->

                        @foreach($products as $product)
                            @if($product->feature)
                                <!-- Best Sellers Item -->
                                    <div class="bestsellers_item discount">
                                        <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                            <div class="bestsellers_image"><img src= {{ url ("images/product/$product->id.png")}} alt=""></div>
                                            <div class="bestsellers_content">
                                                <div class="bestsellers_category"><a href="#">{{$product->name}}</a></div>
                                                <div class="bestsellers_name"><a href="">{{$product->sub_name}}</a></div>
                                                <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                                <div class="bestsellers_price discount">{{$product->price." ".$product->market}}
                                                    @if(!is_null($product->last_price))<span>{{$product->last_price." ".$product->market}}</span>@endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bestsellers_fav "><i class="fas fa-heart"></i></div>
                                        <ul class="bestsellers_marks">
                                            @if(!is_null($product->discount))<li class="bestsellers_mark bestsellers_discount">{{"-".$product->discount." %"}}</li>
                                            @else<li class="bestsellers_mark bestsellers_new active">new</li>@endif
                                        </ul>
                                    </div>
                            @endif
                        @endforeach
                        </div>
                    </div>

                    <div class="bestsellers_panel panel">

                        <!-- Best Sellers Slider -->

                        <div class="bestsellers_slider slider">
                        @foreach($products as $product)
                            @if($product->class==="solde")
                            <!-- Best Sellers Item -->
                            <div class="bestsellers_item discount">
                                <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                    <div class="bestsellers_image"><img src= {{ url ("images/product/$product->id.png")}} alt=""></div>
                                    <div class="bestsellers_content">
                                        <div class="bestsellers_category"><a href="#">{{$product->name}}</a></div>
                                        <div class="bestsellers_name"><a href="">{{$product->sub_name}}</a></div>
                                        <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="bestsellers_price discount">{{$product->price." ".$product->market}}
                                            @if(!is_null($product->last_price))<span>{{$product->last_price." ".$product->market}}</span>@endif
                                        </div>
                                    </div>
                                </div>
                                <div class="bestsellers_fav "><i class="fas fa-heart"></i></div>
                                <ul class="bestsellers_marks">
                                    @if(!is_null($product->discount))<li class="bestsellers_mark bestsellers_discount">{{"-".$product->discount." %"}}</li>
                                    @else<li class="bestsellers_mark bestsellers_new active">new</li>@endif
                                </ul>
                            </div>
                            @endif
                     @endforeach

                        </div>
                    </div>


                        <!-- Best Sellers Slider -->

                    <div class="bestsellers_panel panel">

                        <!-- Best Sellers Slider -->

                        <div class="bestsellers_slider slider">
                        @foreach($products as $product)
                            @if($product->class==="Rechagement")
                                <!-- Best Sellers Item -->
                                    <div class="bestsellers_item discount">
                                        <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                            <div class="bestsellers_image"><img src= {{ url ("images/product/$product->id.png")}} alt=""></div>
                                            <div class="bestsellers_content">
                                                <div class="bestsellers_category"><a href="#">{{$product->name}}</a></div>
                                                <div class="bestsellers_name"><a href="">{{$product->sub_name}}</a></div>
                                                <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                                <div class="bestsellers_price discount">{{$product->price." ".$product->market}}
                                                    @if(!is_null($product->last_price))<span>{{$product->last_price." ".$product->market}}</span>@endif</div>                                            </div>
                                        </div>
                                        <div class="bestsellers_fav"><i class="fas fa-heart"></i></div>
                                        <ul class="bestsellers_marks">
                                            <li class="bestsellers_mark bestsellers_new">new</li>
                                        </ul>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>

                        </div>
            </div>
        </div>
    </div>




<!-- Reviews -->

<div class="reviews">
    <div class="container">
        <div class="row">
            <div class="col">

                <div class="reviews_title_container">
                    <h3 class="reviews_title">Latest Reviews</h3>
                    <div class="reviews_all ml-auto"><a href="#">view all <span>reviews</span></a></div>
                </div>

                <div class="reviews_slider_container">

                    <!-- Reviews Slider -->
                    <div class="owl-carousel owl-theme reviews_slider">

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_1.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Roberto Sanchez</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_2.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Brandon Flowers</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_3.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Emilia Clarke</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_1.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Roberto Sanchez</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_2.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Brandon Flowers</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                        <!-- Reviews Slider Item -->
                        <div class="owl-item">
                            <div class="review d-flex flex-row align-items-start justify-content-start">
                                <div><div class="review_image"><img src= {{ url ("images/review_3.jpg")}} alt=""></div></div>
                                <div class="review_content">
                                    <div class="review_name">Emilia Clarke</div>
                                    <div class="review_rating_container">
                                        <div class="rating_r rating_r_4 review_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="review_time">2 day ago</div>
                                    </div>
                                    <div class="review_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</p></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="reviews_dots"></div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Brands -->

        <!-- Newsletter -->

<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
                    <div class="newsletter_title_container">
                        <div class="newsletter_icon"><img src= {{ url ("images/send.png")}} alt=""></div>
                        <div class="newsletter_title">Sign up for Newsletter</div>
                    </div>
                    <div class="newsletter_content clearfix">
                        <form action="#" class="newsletter_form">
                            <input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
                            <button class="newsletter_button">Subscribe</button>
                        </form>
                        <div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>

</div>

</div>
@include('footer');

    <script src={{ URL::asset("js/jquery-3.3.1.min.js") }}></script>
    <script src={{ URL::asset("styles/bootstrap4/popper.js") }}></script>
    <script src={{ URL::asset("styles/bootstrap4/bootstrap.min.js") }}></script>
    <script src={{ URL::asset("plugins/greensock/TweenMax.min.js") }}></script>
    <script src={{ URL::asset("plugins/greensock/TimelineMax.min.js") }}></script>
    <script src={{ URL::asset("plugins/scrollmagic/ScrollMagic.min.js") }}> </script>
    <script src={{ URL::asset("plugins/greensock/animation.gsap.min.js") }}></script>
    <script src={{ URL::asset("plugins/greensock/ScrollToPlugin.min.js") }}></script>
    <script src={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") }}></script>
    <script src={{ URL::asset("plugins/slick-1.8.0/slick.js") }}></script>
    <script src={{ URL::asset("plugins/easing/easing.js") }}></script>
    <script src={{ URL::asset("js/custom.js") }}></script>
</body>

</html>
