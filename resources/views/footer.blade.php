<!-- Footer -->
<link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" ) }}>
<link rel="stylesheet" type="text/css" href={{ URL::asset("styles/footer.css") }}>
<link rel="stylesheet" type="text/css" href={{ URL::asset("styles/responsive.css") }}>
<script src={!! url("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js") !!}></script>
<script src={!! url("https://ajax.googleapis.com/ajax/libs/d3js/5.15.0/d3.min.js") !!}></script>
<script type="text/javascript" src={!! asset("js/app.js") !!}></script>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 footer_col">
                <div class="footer_column footer_contact">
                        <div  class="footer_title">Contactez-nous</div>
                    <div class="footer_social pt-2">
                        <div class="footer_number">00000000000</div>
                        <div class="footer_number">00000000000</div>
                    </div>
                </div>
                <div class="footer_column footer_contact">
                        <div  class="footer_title pt-2">RETROUVEZ-NOUS SUR</div>
                    <div class="footer_social">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="footer_column footer_contact">
                        <div  class="footer_title">MODES DE PAIEMENT</div>
                    <div class="footer_mode">
                        <ul>
                            <li><img alt="" src={{ url ("images/1.jpg")}}></li>
                            <li><img alt="" src={{ url ("images/Logo_mob.png")}}></li>
                            <li><img alt="" src={{ url ("images/ccp.png")}}></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-lg-3">
                <div class="footer_column">
                    <div class="footer_title">A PROPOS</div>
                    <ul class="footer_list">
                        <li><a href="{{route('guide')}}">Guide d'achat</a></li>
                        <li><a href="#">Qui sommes-nous</a></li>
                        <li><a href="#">Témoignages et Transactions</a></li>
                        <li><a href="#">Grarentie client</a></li>
                        <li><a href="#">Modes de paiement</a></li>
                        <li><a href="#">Modes de livraison</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer_column">
                    <div class="footer_title">Trouvez-le rapidement</div>
                    <ul class="footer_list">
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'solde'])}}">Solde</a></li>
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'abonnement'])}}">Abonnement</a></li>
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'rechargement'])}}">Rechargement</a></li>
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'videos-games'])}}">Video games and credits</a></li>
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'services-electronique'])}}">Services Electronique</a></li>
                        <li class="hassubs"><a href="{{route('shop.index',['category' =>'accessories'])}}">Accessories</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="footer_column">
                    <div class="footer_title">Service client</div>
                    <ul class="footer_list">
                        <li><a href="#">Mon compte</a></li>
                        <li><a href="#">Politique de Confidentialité</a></li>
                        <li><a href="#">Suivre votre commande</a></li>
                        <li><a href="">Service de vente</a></li>
                        <li><a href="{{route('faq')}}">Aide & FAQ</a></li>
                        <li><a href="{{route('contact.index')}}">Contactez Nous</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- Copyright -->

<div class="copyright">
    <div class="container">
        <div class="copyright_content">Icons made by
            <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> ,
            <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> ,
            <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> ,
            <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a>
            from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
        </div>
        <div class="row">
            <div class="col">

                <div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
                    <div class="copyright_content"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                    <div class="logos ml-sm-auto">
                        <ul class="logos_list">
                            <li><a href="#"><img alt=""src={{ url ("images/logos_1.png")}}  ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_2.png")}}   ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_3.png")}}   ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_4.png")}}   ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_5.png")}}   ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_6.png")}}   ></a></li>
                            <li><a href="#"><img alt=""src={{ url ("images/logos_7.png")}}   ></a></li>

                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

