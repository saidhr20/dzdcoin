<!DOCTYPE html>
<html lang="en">
<head>
<title>DZD Cash | {{$product->name}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="DZD CASH">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="dzdcash">
<meta name="keywords" content="dzdcash">
<link rel="stylesheet" type="text/css" href={!! asset("styles/bootstrap4/bootstrap.min.css") !!}>
<link href={!! asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css") !!} rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") !!}>
<link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") !!}>
<link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/animate.css") !!}>
<link rel="stylesheet" type="text/css" href={!! asset("styles/product_styles.css") !!}>
<link rel="stylesheet" type="text/css" href={!! asset("styles/product_responsive.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>
<link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>

</head>

<body>

<div class="super_container">

	<!-- Header -->
    @include('header')

	<!-- Single product -->

	<div class="single_product">
		<div class="container">
            @if(session()->has('success_message'))
                <div class="alert alert-info">{{session()->get('success_message')}}</div>
            @endif
            @if(count($errors) >0 )
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="row">
				<!-- Images -->
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						<li data-image={{ url ("images/single_4.jpg")}}><img src={{ url ("images/single_4.jpg")}} alt=""></li>
						<li data-image={{ url ("images/single_2.jpg")}}><img src={{ url ("images/single_2.jpg")}} alt=""></li>
						<li data-image={{ url ("images/single_3.jpg")}}><img src={{ url ("images/single_3.jpg")}} alt=""></li>
					</ul>
				</div>

				<!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="image_selected"><img src={{ url ("images/product/$product->id.png")}} alt=""></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_category">{{$product->class}}</div>
						<div class="product_name">{{$product->name}}</div>
						<div class="rating_r rating_r_4 product_rating active"><i></i><i></i><i></i><i></i><i></i></div>
						<div class="product_text">{!! $product->description !!}</div>
                        <div class="product_price">{{  $product->price." ".$product->market }}</div>
                            <form action="{{ route('cart.store', $product) }}" method="POST">
                            <div class="qun_price row">
                                <!-- product Quantity-->
									<div class="col-lg-5 col-sm-5 mb-3 product_quantity">
										<span>Quantité :</span>
										<input id="quantity_input" type="number"  pattern="[0-9]*" value="{{$product->min_buy}}" name="quantity" class="quantity">
                                    </div>
                                    <!-- product Color-->
                                    <div class="col-lg-5 col-sm-5 mb-3 product_color">
                                             <span id="price"></span>
									</div>
                            </div>
                                <div class="button_container col-lg-12 col-sm-12">
                                    {!! csrf_field() !!}
                                    @if( is_numeric($product->price))
                                        <button type="submit" class="btn-primary btn-lg btn-block">Ajouter au panier <i class="fas fa-shopping-basket"></i></button>
                                    @else {{$product->price}}
                                @endif
                                </div>
                            </form>

					</div>
				</div>
			</div>
            <div class="viewed">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="viewed_title_container">
                                <h3 class="viewed_title">Vu récemment</h3>
                                <div class="viewed_nav_container">
                                    <div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
                                    <div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
                                </div>
                            </div>

                            <div class="viewed_slider_container">

                                <!-- Recently Viewed Slider -->

                                <div class="owl-carousel owl-theme viewed_slider">

                                    <!-- Recently Viewed Item -->
                                    @foreach($recentViewed as $rs)
                                        <div class="owl-item">
                                            <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                                <div class="viewed_image">
                                                    <a href="{{route('shop.show',$rs->slug)}}">
                                                        <img src={{ url ("images/product/$rs->id.png")}} alt="">
                                                    </a></div>
                                                <div class="viewed_content text-center">
                                                    <div class="viewed_price">{{number_format($rs->price,0,',',' ')." ".$rs->market}}<span>{{$rs->last_price}}</span></div>
                                                    <div class="viewed_name"><a href="#">{{$rs->name}}</a></div>
                                                </div>
                                                <ul class="item_marks">

                                                    <li class="item_mark item_new">new</li>
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>

	<!-- Recently Viewed -->


</div>

	<!-- Footer -->

    @include('footer')
    <script>

        (function () {
            const classname=document.querySelectorAll('.quantity');

            Array.from(classname).forEach(function (element) {
                document.getElementById("price").innerHTML = (element.value*'{{$product->price}}')+" "+"{{$product->market}}";
                element.addEventListener('change',function() {
                 document.getElementById("price").innerHTML = (element.value*'{{$product->price}}')+" "+"{{$product->market}}";



                })

            })

        })();
    </script>


<script src={!! asset("js/jquery-3.3.1.min.js") !!}></script>
<script src={!! asset("styles/bootstrap4/popper.js") !!}></script>
<script src={!! asset("styles/bootstrap4/bootstrap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TweenMax.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TimelineMax.min.js") !!}></script>
<script src={!! asset("plugins/scrollmagic/ScrollMagic.min.js") !!}></script>
<script src={!! asset("plugins/greensock/animation.gsap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/ScrollToPlugin.min.js") !!}></script>
<script src={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") !!}></script>
<script src={!! asset("plugins/easing/easing.js") !!}></script>
<script src={!! asset("js/product_custom.js") !!}></script>
</body>

</html>
