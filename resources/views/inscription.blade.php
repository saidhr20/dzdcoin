<div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registration</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="inscriptions.php" method="post">
                    <div id="status">Remplir les champs</div>
                    <div>
                        <div>
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                            <div><input type="text" name="username" placeholder="username" id="username"></div>
                            <small id="output-username"></small>
                        </div>
                    </div>
                    <div>
                        <div>
                            <span><i class="fas fa-envelope" aria-hidden="true"></i></span>
                            <div><input type="Email" name="email" placeholder="Email" id="email"></div>
                            <small id="output-email"></small>
                        </div>
                    </div>
                    <div>
                        <div>
                            <span><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                            <div><input type="password" name="password" placeholder="Password" id="password"></div>
                            <small id="output-password"></small>
                        </div>
                    </div>
                    <div>
                        <div>
                            <span><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                            <div><input type="password" name="password2" placeholder="Password" id="password2"></div>
                            <small id="output-password2"></small>
                        </div>
                    </div>
                    <div>
                        <span> <input type="checkbox" name="check" required="required"></span>
                        <span> I read and agree for <button> the term of usage</button></span>
                    </div>

                    <div class="captcha"></div>




            </div>
            <div class="modal-footer">
                <div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="signup" class="btn btn-primary">Registre</button>
                    </form>

                </div>
                <div class="col d-flex flex-row">
                    <div><a href="">Forget password</a></div>
                    <div><a href="">Send Activation</a></div>
                    <div><a href="">Sign In</a></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--login-->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                From here !
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Sing In</button>
            </div>
        </div>
    </div>
</div>
