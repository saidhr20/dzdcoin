<!DOCTYPE html>
<html lang="fr">
<head>
    <title>DZD Cash | Formulaire de commande</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/product_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/product_responsive.css">
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>
</head>
<body class="bg-light">


  <div class="super_container">
      @include('header')

	<!-- Header -->
<div class="super_container">
    <div class="container">
        <div class="py-5 text-center">
            <h1>Formulaire de commande</h1>
        </div>
        @if(count($errors) >0 )
            <ul>
                @foreach($errors->all() as $error)
                    <li class="alert alert-danger">
                        {!!$error!!}
                    </li>
                @endforeach
            </ul>
        @endif
        @if(session('error'))
            {{session('error')}}
        @endif
        <div class="row">
            <div class="col-md-5 order-md-2 mb-5">
                <div class="container-perso">
                    <div class="d-flex justify-content-between align-items-center panier_title">
                        <span class="deals_item_name">Ton Panier</span>
                        <span class="badge badge-danger badge-pill">{{Cart::instance('EURO_CART')->content()->count()}} Article </span>
                    </div>
                    <div class="deals_item">
                        <ul class="list-group mb-3">
                            @foreach(Cart::instance('EURO_CART')->content() as $item)
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon"><img src={{url("images/product/$item->id.png")}} alt="" style="width:75px;">
                                            <span class="deals_timer_title">{{$item->name}} </span>
                                        </div>
                                        <span class="badge badge-warning quantity_e"><small>{{$item->qty." x ".$item->price." ".$item->model->market}}</small></span>
                                    </div>
                                    <span class="bestsellers_price discount">{{$item->subtotal(0)." ".$item->model->market}}</span>
                                </li>
                            @endforeach
                                <li class="list-group-item d-flex justify-content-between ">
                                    <span class="dealer_total">Total : </span>
                                    <span class="total_price"><strong>{{Cart::subtotal(0)." ".$item->model->market}}</strong></span>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7 order-md-1">
                <div class="container-information">
                    <h4 class="mb-3">Informations générales</h4>
                    <form action="{{ route('confirmation.store2') }}" method="POST" class="needs-validation" novalidate>
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">Nom <span class="requis">*</span></label>
                                <input type="text" class="form-control" name="first_name" id="firstName" value="{{old('first_name')}}" placeholder="" value="" required>
                                <div class="invalid-feedback">Votre nom valide est requis</div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Prénom <span class="requis">*</span></label>
                                <input type="text" class="form-control" name="last_name" id="lastName" value="{{old('last_name')}}" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                    Votre prénom valide est requis.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="username">Email <span class="requis">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">@</span>
                                </div>
                                @if(auth()->user())
                                    <input type="email" class="form-control" name="email" id="Email" value="{{auth()->user()->email}}" placeholder="example@exemple.com" readonly>
                                @else
                                    <input type="email" class="form-control" name="email" id="Email" value="{{old('email')}}" placeholder="example@exemple.com" required>
                                @endif
                                <div class="invalid-feedback" style="width: 100%;">
                                    Votre email valide est requis.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="address">Adresse <span class="requis">*</span></label>
                            <input type="text" class="form-control" name="address" id="address" value="{{old('address')}}" placeholder="" required>
                            <div class="invalid-feedback">
                                Veuillez entrer votre adresse.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="state">Wilaya <span class="requis">*</span></label>
                                <select class="custom-select d-block w-100" id="state" name="wilaya" value="{{old('wilaya')}}" required>
                          <option value="">Choose...</option>
                          <option>1 - ADRAR</option>
                          <option>2 - CHLEF</option>
                          <option>3 - LAGHOUAT</option>
                          <option>4 - OUM BOUAGHI</option>
                          <option>5 - BATNA</option>
                          <option>6 - BEJAIA </option>
                          <option>7 - BISKRA</option>
                          <option>8 - BECHAR</option>
                          <option>9 - BLIDA</option>
                          <option>10 - BOUIRA</option>
                          <option>11 - TAMANRASSET</option>
                          <option>12 - TEBESSA</option>
                          <option>13 - TLEMCEN</option>
                          <option>14 - TIARET</option>
                          <option>15 - TIZI OUZOU</option>
                          <option>16 - ALGER</option>
                          <option>17 - DJELFA</option>
                          <option>18 - JIJEL</option>
                          <option>19 - SETIF</option>
                          <option>20 - SAIDA</option>
                          <option>21 - SKIKDA</option>
                          <option>22 - SIDI BEL ABBES</option>
                          <option>23 - ANNABA</option>
                          <option>24 - GUELMA</option>
                          <option>25 - CONSTANTINE</option>
                          <option>26 - MEDEA</option>
                          <option>27 - MOSTAGANEM</option>
                          <option>28 - M'SILA</option>
                          <option>29 - MASCARA</option>
                          <option>30 - OUARGLA</option>
                          <option>31 - ORAN</option>
                          <option>32 - EL BAYDH</option>
                          <option>33 - ILLIZI</option>
                          <option>34 - BORDJ BOU ARRERIDJ</option>
                          <option>35 - BOUMERDES</option>
                          <option>36 - EL TAREF</option>
                          <option>37 - TINDOUF</option>
                          <option>38 - TISSEMSILT</option>
                          <option>39 - EL OUED</option>
                          <option>40 - KHENCHLA</option>
                          <option>41 - SOUK AHRASS</option>
                          <option>42 - TIPAZA</option>
                          <option>43 - MILA</option>
                          <option>44 - AÏN DEFLA</option>
                          <option>45 - NÂAMA</option>
                          <option>46 - AÏN TEMOUCHENT</option>
                          <option>47 - GHARDAÏA</option>
                          <option>48 - RELIZANE</option>
                                </select>
                                <div class="invalid-feedback">
                                    Veuillez entrer votre wilaya.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="zip">Code postal <span class="requis">*</span></label>
                                <input type="text" class="form-control" name="postal" pattern="[0-9]*" id="zip" placeholder="" value="{{old('postal')}}" required>
                                <div class="invalid-feedback">
                                    Code postal est requis.
                                </div>
                            </div>
              </div>
                        <div class="mb-3">
                            <label for="phone">Numéro de téléphone <span class="requis">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="phone" id="phone" pattern="[0-9]*" value="{{old('phone')}}" placeholder="" required>
                                <div class="invalid-feedback" style="width: 100%;">
                                    Numéro de téléphone est requis.
                                </div>
                            </div>
                        </div>
              <hr class="mb-4">
                        <label class="mb-2">Mode de paiement <span class="requis">*</span></label>
                            <div class="d-block my-3">
                              <div class="custom-control custom-radio">
                                <input  id="paysera" name="paymentMethod" value="paysera" type="radio" class="custom-control-input" checked required>
                                <label class="custom-control-label" for="paysera">Paysera</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input id="paypal" name="paymentMethod" value="paypal" type="radio" class="custom-control-input" required>
                                <label class="custom-control-label" for="paypal">Paypal</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input  id="main" name="paymentMethod" value="main" type="radio" class="custom-control-input" required>
                                <label class="custom-control-label" for="main">Autre</label>
                              </div>
                            </div>
                        <hr class="mb-4">
                        <div class="mb-3">
                            <label for="additional">Information additionnelle</label>
                            <div class="input-group">
                                <textarea class="form-control" name="additional"></textarea>
                            </div>
                        </div>
                        <hr class="mb-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="save-info">
                            <label class="custom-control-label" for="save-info">Enregistrez ces informations pour la prochaine fois</label>
                        </div>
                        <hr class="mb-4">
                        {!! Form::submit('Commander',['class'=>'btn-primary btn-lg btn-block']) !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

  </div>
    @include('footer')


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>
    <script>
        // Coloring a selected radio
        $("#paysera").on( "click", function () {
            $("#paysera-payment").css("background-color","#56c1ab");
            $("#Paypal-payement").css("background-color","unset");
        } );
        $("#paypal").on( "click", function () {
            $("#Paypal-payement").css("background-color","#56c1ab");
            $("#paysera-payment").css("background-color","unset");
        } );


      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
  </body>
</html>

