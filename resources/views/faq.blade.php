<!DOCTYPE html>
<html lang="en">
<head>
    <title>DZD Cash | FAQ</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="stylesheet" type="text/css" href={!! asset("styles/bootstrap4/bootstrap.min.css") !!}>
    <link href={!! asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css") !!} rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/animate.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_styles.css") !!}>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_responsive.css") !!}>
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>
</head>

<body>


<div class="super_container">

    <!-- Header -->
    @include('header')

    <div class="container guide">
        <div class="guide-title">
                <h1>Questions Fréquentes (F.A.Q)</h1>
            </div>
            <ul class="nav nav-pills"  id="myTab" role="tablist">
                <li class="nav-item py-2">
                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">Générale</a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" id="pop-tab" data-toggle="tab" href="#pop" role="tab" aria-controls="pop" aria-selected="false">Populaire</a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" id="achat-tab" data-toggle="tab" href="#achat" role="tab" aria-controls="achat" aria-selected="false">Achat</a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" id="vente-tab" data-toggle="tab" href="#vente" role="tab" aria-controls="vente" aria-selected="false">Vente</a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" id="conf-tab" data-toggle="tab" href="#conf" role="tab" aria-controls="conf" aria-selected="false">Confidentialité</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12  beg" >
                            <div class="accordion" id="faqExample">
                                <div class="question-reponse">
                                    <div class="card-header p-2" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Q: Comment j'acheter un produit ?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#faqExample">
                                        <div class="card-body">
                                            <b>Answer:</b> It works using the Bootstrap 4 collapse component with cards to make a vertical accordion that expands and collapses as questions are toggled.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-2" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Q: Comment coûtent les frais de livraison ?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                        <div class="card-body">
                                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-2" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Q: Combien de temps prend la livraison ?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-2" id="heading5">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                Q: Est ce que je peux annuler ma commande?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-2" id="heading6">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                Q: comment tracker ma commande ?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pop" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12  beg" >
                            <div class="accordion" id="faqExample">
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Q: How does this work?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#faqExample">
                                        <div class="card-body">
                                            <b>Answer:</b> It works using the Bootstrap 4 collapse component with cards to make a vertical accordion that expands and collapses as questions are toggled.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Q: What is Bootstrap 4?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                        <div class="card-body">
                                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="achat" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12  beg" >
                            <div class="accordion" id="faqExample">
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Q: How does this work?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#faqExample">
                                        <div class="card-body">
                                            <b>Answer:</b> It works using the Bootstrap 4 collapse component with cards to make a vertical accordion that expands and collapses as questions are toggled.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Q: What is Bootstrap 4?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                        <div class="card-body">
                                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="vente" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12  beg" >
                            <div class="accordion" id="faqExample">
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Q: How does this work?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#faqExample">
                                        <div class="card-body">
                                            <b>Answer:</b> It works using the Bootstrap 4 collapse component with cards to make a vertical accordion that expands and collapses as questions are toggled.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Q: What is Bootstrap 4?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                        <div class="card-body">
                                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="conf" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12  beg" >
                            <div class="accordion" id="faqExample">
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Q: How does this work?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#faqExample">
                                        <div class="card-body">
                                            <b>Answer:</b> It works using the Bootstrap 4 collapse component with cards to make a vertical accordion that expands and collapses as questions are toggled.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Q: What is Bootstrap 4?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                        <div class="card-body">
                                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>
                                <div class="question-reponse">
                                    <div class="card-header p-1" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed btn-lg" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                Q. What is another question?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#faqExample">
                                        <div class="card-body">
                                            The answer to the question can go here.
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--/row-->
        </div>
        <!--container-->
    </div>
<div>
</div>

@include('footer')


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script !src="">
    $("#pop-tab,#general-tab,#vente-tab,#achat-tab,#conf-tab").on( "click", function () {
        $("html, body").animate({
            scrollTop: $("#myTab").offset().top
        });
    } );

</script>
<script src={!! asset("js/jquery-3.3.1.min.js") !!}></script>
<script src={!! asset("styles/bootstrap4/popper.js") !!}></script>
<script src={!! asset("styles/bootstrap4/bootstrap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TweenMax.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TimelineMax.min.js") !!}></script>
<script src={!! asset("plugins/scrollmagic/ScrollMagic.min.js") !!}></script>
<script src={!! asset("plugins/greensock/animation.gsap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/ScrollToPlugin.min.js") !!}></script>
<script src={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") !!}></script>
<script src={!! asset("plugins/easing/easing.js") !!}></script>
<script src={!! asset("js/product_custom.js") !!}></script>
</body>
</html>

