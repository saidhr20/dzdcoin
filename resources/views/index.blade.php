<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>DZD Cash | Home</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/bootstrap4/bootstrap.min.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" ) }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/OwlCarousel2-2.2.1/animate.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/slick-1.8.0/slick.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/header.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/main_styles.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/responsive.css") }}>
    <script data-ad-client="ca-pub-6806108458634058" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
<div id="loader-wrapper"  id="content">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<div class="super_container">
    @include('header')
    <!--promotion de semaine-->
        <div class="container">
            <div class="row">
                <!-- Deals of the week -->
                <div class="deals_featured">
                    <div class="container">
                        <div class="row">
                            <div class="col d-flex flex-lg-row flex-column align-items-center justify-content-start">
                                <!-- Featured -->
                                <div class="featured">
                                    <div class="tabbed_container">
                                        <div class="tabs">
                                            <ul class="clearfix">
                                                <li class="active">Les plus demandés</li>
                                            </ul>
                                            <div class="tabs_line"><span></span></div>
                                        </div>
                                        <!-- product Panel -->
                                        <div class="product_panel panel active">
                                            <div class="featured_slider slider">
                                            @foreach($product as  $prod)
                                                @if($prod->feature)
                                                    <!-- Slider Item -->
                                                        <div class="featured_slider_item">
                                                            <div class="border_active"></div>
                                                            <div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                                                <div class="product-class">
                                                                    <div class="product_image d-flex flex-column align-items-center justify-content-center">
                                                                        <a href="{{route('shop.show',$prod->slug)}}" class="image_product_featured"><img src= {{ url ("images/product/$prod->id.png")}} alt=""></a></div>
                                                                    <div class="product_content">
                                                                        <div class="product_price discount">
                                                                            {{ number_format($prod->price, 0, ',', ' ')." ".$prod->market }}
                                                                            <span>@if($prod->stock>0){!!('<span class="badge badge-secondary">'.$prod->stock." ".$prod->etq.'</span>')!!}@else
                                                                                    {!!('<span class="badge badge-danger">Rupture</span>')!!}@endif</span></div>
                                                                        <div class="product_name"><a href="{{route('shop.show',$prod->slug)}}" >{{  $prod->name  }} </a></div></div>
                                                                </div>
                                                                <div class="product_extras">
                                                                    <form action="{{ route('cart.store', $prod) }}" method="POST">
                                                                        {!! csrf_field() !!}
                                                                        <button type="submit" class="product_cart_button">Ajouter au panier <i class="fas fa-shopping-basket"></i></button>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                            <div class="product_fav"><i class="fas fa-heart"></i></div>
                                                            <ul class="product_marks">
                                                                <li class="product_mark product_discount">{{$prod->discount }} % </li>
                                                                <li class="product_mark product_new">new</li>
                                                            </ul>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>






                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Popular Categories -->
        <div class="characteristics">
            <div class="container">
            <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="char_item text-center">
                            <div class="char_icon"><img src= {{ url ("images/char_7.png")}} alt=""></div>
                            <div class="char_content">
                                <div class="char_title">Livraison Rapide</div>
                                <div class="char_subtitle">Recevez vos produits en 5 minutes jusqu'à 12h maximum </div>
                            </div>
                        </div>
                    </div>

                    <!-- Char. Item -->
                    <div class="col-lg-3 col-md-3">
                        <div class="char_item text-center">
                            <div class="char_icon"><img src= {{ url ("images/char_6.png")}} alt=""></div>
                            <div class="char_content">
                                <div class="char_title">Satisfaction Garantie</div>
                                <div class="char_subtitle">Votre argent est en garanti total jusqu'à ce que vous receviez votre service</div>
                            </div>
                        </div>
                    </div>
                    <!-- Char. Item -->
                    <div class="col-lg-3 col-md-3">
                        <div class="char_item text-center">
                            <div class="char_icon"><img src= {{ url ("images/char_8.png")}} alt=""></div>
                            <div class="char_content">
                                <div class="char_title">Des produits digitaux</div>
                                <div class="char_subtitle">À des prix exceptionnels</div>
                            </div>
                        </div>
                    </div>

                    <!-- Char. Item -->
                    <div class="col-lg-3 col-md-3">
                        <div class="char_item text-center">
                            <div class="char_icon"><img src= {{ url ("images/char_5.png")}} alt=""></div>
                            <div class="char_content">
                                <div class="char_title">SUPPORT EN LIGNE 12/7</div>
                                <div class="char_subtitle">Assistance en ligne disponible </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>



        <!-- Best Sellers -->

        <div class="best_sellers">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="tabbed_container">
                            <div class="tabs clearfix tabs-right">
                                <div class="new_arrivals_title">Top sélection</div>
                                <ul class="clearfix">
                                    <li class="active">Produits recommandées</li>
                                </ul>
                                <div class="tabs_line"><span></span></div>
                            </div>

                            <div class="bestsellers_panel panel active">

                                <!-- Best Sellers Slider -->

                                <div class="bestsellers_slider slider">

                                    <!-- Best Sellers Item -->

                                @foreach($product as $prod)
                                    @if($prod->feature)
                                        <!-- Best Sellers Item -->
                                            <div class="bestsellers_item discount">
                                                <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                                    <div class="bestsellers_image"><img src= {{ url ("images/product/$prod->id.png")}} alt=""></div>
                                                    <div class="bestsellers_content">
                                                        <div class="bestsellers_category"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->name}}</a></div>
                                                        <div class="bestsellers_name"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->sub_name}}</a></div>
                                                        <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                                        <div class="bestsellers_price discount">{{$prod->price." ".$prod->market}}
                                                            @if(!is_null($prod->last_price))<span>{{$prod->last_price." ".$prod->market}}</span>@endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bestsellers_fav "><i class="fas fa-heart"></i></div>
                                                <ul class="bestsellers_marks">
                                                    @if(!is_null($prod->discount))<li class="bestsellers_mark bestsellers_discount">{{"-".$prod->discount." %"}}</li>
                                                    @else<li class="bestsellers_mark bestsellers_new active">new</li>@endif
                                                </ul>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="bestsellers_panel panel">

                                <!-- Best Sellers Slider -->

                                <div class="bestsellers_slider slider">
                                @foreach($product as $prod)
                                    @if($prod->class==="solde")
                                        <!-- Best Sellers Item -->
                                            <div class="bestsellers_item discount">
                                                <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                                    <div class="bestsellers_image"><img src= {{ url ("images/product/$prod->id.png")}} alt=""></div>
                                                    <div class="bestsellers_content">
                                                        <div class="bestsellers_category"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->name}}</a></div>
                                                        <div class="bestsellers_name"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->sub_name}}</a></div>
                                                        <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                                        <div class="bestsellers_price discount">{{$prod->price." ".$prod->market}}
                                                            @if(!is_null($prod->last_price))<span>{{$prod->last_price." ".$prod->market}}</span>@endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bestsellers_fav "><i class="fas fa-heart"></i></div>
                                                <ul class="bestsellers_marks">
                                                    @if(!is_null($prod->discount))<li class="bestsellers_mark bestsellers_discount">{{"-".$prod->discount." %"}}</li>
                                                    @else<li class="bestsellers_mark bestsellers_new active">new</li>@endif
                                                </ul>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>


                            <!-- Best Sellers Slider -->

                            <div class="bestsellers_panel panel">

                                <!-- Best Sellers Slider -->

                                <div class="bestsellers_slider slider">
                                @foreach($product as $prod)
                                    @if($prod->class==="Rechagement")
                                        <!-- Best Sellers Item -->
                                            <div class="bestsellers_item discount">
                                                <div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
                                                    <div class="bestsellers_image"><img src= {{ url ("images/product/$prod->id.png")}} alt=""></div>
                                                    <div class="bestsellers_content">
                                                        <div class="bestsellers_category"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->name}}</a></div>
                                                        <div class="bestsellers_name"><a href="{{route('shop.show',$prod->slug)}}">{{$prod->sub_name}}</a></div>
                                                        <div class="rating_r rating_r_4 bestsellers_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                                        <div class="bestsellers_price discount">{{$prod->price." ".$prod->market}}
                                                            @if(!is_null($prod->last_price))<span>{{$prod->last_price." ".$prod->market}}</span>@endif</div>                                            </div>
                                                </div>
                                                <div class="bestsellers_fav"><i class="fas fa-heart"></i></div>
                                                <ul class="bestsellers_marks">
                                                    <li class="bestsellers_mark bestsellers_new">new</li>
                                                </ul>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>




            <!-- Reviews -->

            <div class="reviews">
                <div class="container">
                    <div class="row">
                        <div class="col">

                            <div class="reviews_title_container">
                                <h3 class="reviews_title">derniers commentaire</h3>
                                <div class="reviews_all ml-auto"><a href="#">voir toutes les <span>reviews</span></a></div>
                            </div>

                            <div class="reviews_slider_container">

                                <!-- Reviews Slider -->
                                <div class="owl-carousel owl-theme reviews_slider">

                                    @foreach($reviews as $review)
                                    <div class="owl-item">
                                        <div class="review d-flex flex-row align-items-start justify-content-start">
                                            <div><div class="review_image"><img src=" {{ url ('storage/'.$review->image)}}" alt=""></div></div>
                                            <div class="review_content">
                                                <div class="review_name">{{$review->name}}</div>
                                                <div class="review_rating_container">
                                                    <div class="rating_r rating_r_4 review_rating">
                                                        @for($i=0;$i<$review->rating;$i++)
                                                        <i></i>
                                                            @endfor
                                                    </div>
                                                    <div class="review_time">{{$review->created_at->diffForHumans() }}</div>
                                                </div>
                                                <div class="review_text"><p>{!!  $review->reviewing!!}</p></div>
                                            </div>
                                        </div>
                                    </div>

                                        @endforeach
                                </div>
                                <div class="reviews_dots"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('footer')

<script src={{ URL::asset("js/jquery-3.3.1.min.js") }}></script>
<script src={{ URL::asset("styles/bootstrap4/popper.js") }}></script>
<script src={{ URL::asset("styles/bootstrap4/bootstrap.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/TweenMax.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/TimelineMax.min.js") }}></script>
<script src={{ URL::asset("plugins/scrollmagic/ScrollMagic.min.js") }}> </script>
<script src={{ URL::asset("plugins/greensock/animation.gsap.min.js") }}></script>
<script src={{ URL::asset("plugins/greensock/ScrollToPlugin.min.js") }}></script>
<script src={{ URL::asset("plugins/OwlCarousel2-2.2.1/owl.carousel.js") }}></script>
<script src={{ URL::asset("plugins/slick-1.8.0/slick.js") }}></script>
<script src={{ URL::asset("plugins/easing/easing.js") }}></script>
<script src={{ URL::asset("js/custom.js") }}></script>
</body>

</html>
