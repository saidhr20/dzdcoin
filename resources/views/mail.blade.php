<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>confirmation payment</title>
    <style>
        /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
        * {
            margin: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {

            font-family: 'Rubik', sans-serif;
            font-size: 14px;
            font-weight: 400;
            background: #FFFFFF;
            color: #000000;
        }

        /* Let's make sure all tables have defaults */
        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #f6f6f6;
        }

        .body-wrap {
            background-color: #f6f6f6;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background-color: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 3px;
        }

        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .footer {
            width: 100%;
            clear: both;
            color: #999;
            padding: 20px;
        }
        .footer p, .footer a, .footer td {
            color: #999;
            font-size: 12px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            color: #000;
            margin: 40px 0 0;
            line-height: 1.2em;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
            /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
            /*line-height: 38px;*/
        }

        h2 {
            font-size: 24px;
            /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
            /*line-height: 29px;*/
        }

        h3 {
            font-size: 18px;
            /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
            /*line-height: 22px;*/
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
        }
        p li, ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            color: #348eda;
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #348eda;
            border: solid #348eda;
            border-width: 10px 20px;
            line-height: 2em;
            /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
            /*line-height: 28px;*/
            font-weight: bold;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 5px;
            text-transform: capitalize;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .aligncenter {
            text-align: center;
        }

        .alignright {
            text-align: right;
        }

        .alignleft {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        /* -------------------------------------
            ALERTS
            Change the class depending on warning email, good email or bad email
        ------------------------------------- */
        .alert {
            font-size: 16px;
            color: #fff;
            font-weight: 500;
            padding: 20px;
            text-align: center;
            border-radius: 3px 3px 0 0;
        }
        .alert a {
            color: #fff;
            text-decoration: none;
            font-weight: 500;
            font-size: 16px;
        }
        .alert.alert-warning {
            background-color: #FF9F00;
        }
        .alert.alert-bad {
            background-color: #D0021B;
        }
        .alert.alert-good {
            background-color: #68B90F;
        }

        /* -------------------------------------
            INVOICE
            Styles for the billing table
        ------------------------------------- */
        .invoice {
            margin: 40px auto;
            text-align: left;
            width: 100%;

            border: 1px #000 solid;
        }
        .invoice td {
            padding: 5px 0;
            padding: 15px;
        }
        .invoice .invoice-items {
            width: 100%;
        }
        .invoice .invoice-items td {
            border-top: #afafaf 1px solid;

        }
        .invoice .invoice-items .total td {
            border-top: 2px solid #333;
            border-bottom: 2px solid #333;
            font-weight: 700;
            padding: 5px;


        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            body {
                padding: 0 !important;
            }

            h1, h2, h3, h4 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                padding: 0 !important;
                width: 100% !important;
            }

            .content {
                padding: 0 !important;
            }

            .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }
    </style>
    <link rel="stylesheet" href={!! asset("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css") !!} integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"  crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href={{ URL::asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" ) }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/footer.css") }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset("styles/responsive.css") }}>
</head>
<body itemscope itemtype="http://schema.org/EmailMessage">

<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">

                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="border: 1px #000 solid">
                    <tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-block">
                                        <div class="text-center">
                                            <img src="{{ url ("images/shop.jpg")}}" class="img-fluid">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <p class="aligncenter" style="margin-top: 30px;font-size: 16px">Cher(e) hera,
                                            Vous avez récemment passé commande et nous vous en remercions!
                                            Votre commande 305578612 est en attente de confirmation,
                                            confirmez votre commande pour passer a l'étape de paiement
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block aligncenter">
                                        <table class="invoice">
                                            <tr>
                                                <td>Lee Munroe<br>Invoice #12345<br>June 01 2014</td>
                                            </tr>
                                            <tr>
                                                <div class="container-personaliser">
                                                    <div class="d-flex justify-content-between">
                                                        <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                            <span class="deals_item_name active">Verser à</span>
                                                        </h4>
                                                        <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                            <span class="deals_item_name active"> :ادفع الى</span>
                                                        </h4>
                                                    </div>
                                                    <div class="deals_item">
                                                        <ul class="list-group mb-3">
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <div class="char_icon">
                                                                        <i class="fas fa-user"></i> <span class="deals_timer_title">Nom & Prénom</span>
                                                                    </div>

                                                                </div>
                                                                <span class="bestsellers_price discount">HADJ RAMDANE SAID</span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <div class="char_icon">
                                                                        <i class="fas fa-user"></i> <span class="deals_timer_title">الاسم و القلب</span>
                                                                    </div>

                                                                </div>
                                                                <span class="bestsellers_price discount">حاج رمضان سعيد</span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <div class="char_icon">
                                                                        <i class="fas fa-envelope"></i>  <span class="deals_timer_title">Compte CCP</span>
                                                                    </div>

                                                                </div>
                                                                <span class="bestsellers_price discount">0020681067 CLE 68</span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <div class="char_icon">
                                                                        <i class="fas fa-phone-square"></i> <span class="deals_timer_title">Num° Tel</span>
                                                                    </div>

                                                                </div>
                                                                <span class="bestsellers_price discount">0000000000</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </tr>
                                            <tr>
                                                <div class="alert-light" ><span class="badge badge-pill"><h5>[#0000]  رقم التتبع : </h5></span> اكتب على الوصل </div>
                                                <div class="alert-light"><span class="badge badge-pill"><h4>saidhr20@gmail.com</h4></span>: أرسل الوصل الى البريد الالكتروني </div>
                                                <div class="alert-warning" style="margin: 10px"><h5>  ملاحظة : الدفع من الساعة 09.00 صباحا الى 15.00 مساءا</h5></div>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="invoice-items" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <div class="char_icon"><img src={{url("images/product/1.png")}} alt="" style="width:75px;">
                                                                    <div style="padding-left: 15px"><span class="badge badge-secondary">paysera </span></div>
                                                                </div>
                                                            </td>

                                                            <td class="alignright" >
                                                                <div class="text-right" style="margin: 10px">Qté: 15$</div>
                                                                <h5><span class="badge badge-success">2700 DZD </span></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="char_icon"><img src={{url("images/product/3.png")}} alt="" style="width:75px;">
                                                                    <div style="padding-left: 15px"><span class="badge badge-secondary">Skrill </span></div>
                                                                </div>
                                                            </td>
                                                            <td class="alignright" >
                                                                <div class="text-right" style="margin: 10px">Qté: 2</div>
                                                                <h5><span class="badge badge-success">5000 DZD </span></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="char_icon"><img src={{url("images/product/2.png")}} alt="" style="width:75px;">
                                                                    <div style="padding-left: 15px"><span class="badge badge-secondary">neteller</span></div>
                                                                </div>
                                                            </td>
                                                            <td class="alignright">
                                                                <div class="text-right"  style="margin: 10px">Qté: 80$</div>
                                                                <h5><span class="badge badge-success">15000 DZD </span></h5>
                                                            </td>
                                                        </tr>
                                                        <tr class="total" style="width: 100%;">
                                                            <td class="alignright">
                                                            </td>
                                                            <td class="alignleft">
                                                                <h4 class="text-right" style="margin: 10px 00px 10px 0px">Total : 27000 DZD</h4>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block" style="padding: 0">
                                        <div class="footer_social">
                                            <ul style="list-style: none;justify-content:center">
                                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block">Questions? Email <a href="mailto:">support@dzdcash.com</a></td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>
