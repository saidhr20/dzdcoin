<!DOCTYPE html>
<html lang="en">
<head>
    <title>DZD Cash | Panier EURO</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DZD CASH">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="dzdcash">
    <meta name="keywords" content="dzdcash">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="styles/cart_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/cart_responsive.css">
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>

</head>

<body>

<div class="super_container">
    @include('header')

<!-- Header -->



<!-- Cart -->


<div class="cart_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart_container">
                    <div class="cart_title">Panier en devise</div>
                    <div>
                        @if(session()->has('success_message'))
                            <div class="alert alert-success">{{session()->get('success_message')}}</div>
                        @endif
                        @if(session()->has('info_message'))
                            <div class="alert alert-warning">{{session()->get('info_message')}}</div>
                        @endif
                            @if(session()->has('error_message'))
                                <div class="alert alert-danger">{!!session()->get('error_message')!!}</div>
                            @endif
                            @if(session()->has('errors_qty'))
                                <div class="alert alert-danger">{!!session()->get('errors_qty')!!}</div>
                            @endif
                        @if(count($errors) >0 )
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    @if(Cart::instance('EURO_CART')->count()<1)
                        <div class="justify-content-md-center ">
                            <div class="cart_vide col-lg-8 col-md-6">
                                <div><img src= {{ url ("images/cartvide.png")}} alt=""></div>
                                <h2>Votre panier est vide !</h2>
                                Vous avez déjà un compte? <a href="">Connectez-vous</a> pour voir les articles dans votre panier.
                                <div class="cart_button">
                                    <a href="{{route('shop.index')}}" type="button" class="btn btn-primary">Retour à la boutique</a>
                                </div>
                            </div>
                        </div>

                    @else
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">
                                    <div class="cart_item_title">Icône</div></th>
                                <th scope="col">
                                    <div class="cart_item_title">L'Article</div>
                                </th>
                                <th scope="col"><div class="cart_item_title">Quantité</div>
                                </th>
                                <th scope="col"><div class="cart_item_title">Prix</div>
                                </th>
                                <th scope="col"><div class="cart_item_title">Total</div>
                                </th>
                                <th scope="col"><div class="cart_item_title">Action</div>
                                </th>
                            </tr>
                            </thead>
                            @foreach(Cart::instance('EURO_CART')->content() as $item)
                                <tbody>
                                <tr>
                                    <th>
                                        <div class="cart_item_image">
                                            <a href={{route('shop.show',$item->model->slug)}}>
                                                <img src="{{ asset("images/product/".$item->model->id.".png") }}" alt="">
                                            </a>
                                        </div>
                                    </th>
                                    <td>
                                        <div class="cart_item_text">
                                            <a href={{route('shop.show',$item->model->slug)}}>{{$item->model->name}}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="cart_item_text">
                                            <input type="number" name="{{$item->model->name}}" data-id="{{$item->rowId}}" min="{{$item->model->min_buy}}" max="{{$item->model->max_buy}}"  class="quantity" value="{{$item->qty}}">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="cart_item_text">
                                            {{$item->model->price." ".$item->model->market}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="cart_item_text">
                                            {{$item->subtotal(0)." ".$item->model->market}}
                                        </div>
                                    </td>
                                    {{ Form::open(['route' => ['carte.destroy2',$item->rowId],'action' => 'CartController@destroy2']) }}
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <td>
                                        <div class="cart_item_text">
                                            <button type="submit" class="fas fa-trash-alt" id="trash-button"></button>
                                        </div>
                                    </td>
                                    {!! Form::close() !!}
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        </div>
                    <!-- Order Total -->
                    <div class="order_total">
                        <div class="order_total_content text-md-right">
                            <div class="order_total_title">Total de la commande: </div>
                            <div class="order_total_amount">{{Cart::instance('EURO_CART')->subtotal(1)}} €</div>
                        </div>
                    </div>
                    <div class="cart_buttons">
                        <div class="py-2">
                            <a href="{{ route('carte.create') }}" type="button" class="button cart_button_clear">Mettre à jour le prix <i class="fas fa-sync-alt"></i> </a>
                        </div>
                        <div class="py-2">
                            <a href="{{route('shop.index')}}" type="button" class="button cart_button_back">Ajouter d'autre(s) article(s) <i class="fas fa-cart-plus"></i> </a>
                        </div>
                        <div class="py-2">
                            <a href="{{route('checkout.create')}}" type="button" class="button cart_button_checkout">Procéder au paiement <i class="far fa-hand-point-right"></i></a>
                        </div>
                    </div>
                    @endif
                </div>

            </div>

        </div>

    </div>

</div>
</div>
<!-- Newsletter -->
<script>
    (function () {
        const classname=document.querySelectorAll('.quantity');
        Array.from(classname).forEach(function (element) {
            element.addEventListener('change',function()
            {
                const id=element.getAttribute('data-id');
                axios.patch(`/carte/${id}`, {
                    quantity: this.value,
                    name: this.name,
                    min:this.min,
                    max:this.max,
                })
                    .then(function (response) {
                        // console.log(response);

                    })
                    .catch(function (error) {
                        console.log(error);

                    });
            })

        })

    })();
</script>
<!-- Footer -->
@include('footer')
</body>

<script src={!! asset("js/jquery-3.3.1.min.js") !!}></script>
<script src={!! asset("styles/bootstrap4/popper.js") !!}></script>
<script src={!! asset("styles/bootstrap4/bootstrap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TweenMax.min.js") !!}></script>
<script src={!! asset("plugins/greensock/TimelineMax.min.js") !!}></script>
<script src={!! asset("plugins/scrollmagic/ScrollMagic.min.js") !!}></script>
<script src={!! asset("plugins/greensock/animation.gsap.min.js") !!}></script>
<script src={!! asset("plugins/greensock/ScrollToPlugin.min.js") !!}></script>
<script src={!! asset("plugins/easing/easing.js") !!}></script>
<script src={!! asset("js/cart_custom.js") !!}></script>
<script src={!! asset("js/app.js") !!}></script>

</html>

