@extends('layouts.header')

@section('content')
    <div class="container_auth" >
    <div class="row justify-content-center">
        <div class="col-lg-7 col-sm-12">
            <div class="card">
                <div class="card-header text-center">{{ __('Créer un compte') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf


                            <!-- div class="input-container col-md-4 offset-1 col-form-label text-md-right" >
                                <input id="prenom" type="text"  class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  required autocomplete="name" autofocus/>
                                <label for="prenom">{{ __('Prénom') }}</label>
                                <div class="bar">
                                    @error('Name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div -->

                                <div class="form-group row">
                                    <div class="input-container col-lg-7 col-sm-6 col-form-label text-md-left">
                                <input id="name" type="text"  class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  required autocomplete="name" autofocus/>
                                <span class="label_fa"><i class="fas fa-user"></i></span>
                                <label for="name">{{ __('Nom') }}</label>
                                <div class="bar"> </div>
                                    @error('Name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-container col-lg-7 col-sm-6 col-form-label text-md-left">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                                <span class="label_fa"><i class="fas fa-envelope"></i></span>
                                <label for="email">{{ __('Adresse e-mail') }}</label>
                                <div class="bar"></div>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <!-- div class="input-container col-md-4 offset-1 col-form-label text-md-right" >
                                <input id="phone" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="number" autofocus/>
                                <label for="phone">{{ __('Téléphone mobile (optionnel)') }}</label>
                                <div class="bar">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div !-->
                        </div>

                        <div class="form-group row">
                            <div class="input-container col-lg-7 col-sm-6 col-form-label text-md-left" >
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <span class="label_fa"><i class="fas fa-lock"></i></span>
                                <label for="password">{{ __('Mot de passe') }}</label>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                                <div class="bar"></div>

                             </div>
                        </div>
                        <div class="form-group row">
                            <div class="input-container col-lg-7 col-sm-6 col-form-label text-md-right" >
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                                <span class="label_fa"><i class="fas fa-lock"></i></span>
                                <label for="password-confirm" >{{ __('Confirmer mot de passe') }}</label>
                                <div class="bar"></div>
                            </div>
                        </div>
                        <div class="form-group row text-right">
                        <div class="custom-control custom-checkbox col-md-9 text-center">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">J'ai lu et j'accepte les  <a href="#">Conditions générales de vente</a></label>
                        </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 col-sm-4 col-form-label text-md-center">
                                    <button type="submit" class="button_login" >
                                        <span><i class="fas fa-sign-in-alt"></i></span>{{ __('Créer votre compte') }}
                                    </button>
                                <hr>
                                <a href="#">
                                    <button type="btn" class="button_login_facebook">
                                    <span><i class="fab fa-facebook-square"></i></span>{{ __('Créer votre compte avec Facebook') }}
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 col-sm-4 col-form-label text-center">
                                {{ __('Vous possédez déjà un compte ?') }}
                            </div>
                            <div class="col-md-8 col-sm-4 col-form-label text-center">
                                <a href="{{route('login')}}">{{ __('CONNECTEZ-VOUS') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
