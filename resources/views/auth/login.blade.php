@extends('layouts.header')
@section('content')
<div class="container_auth row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Se connecter') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="cont_form">
                      <div class="form-group row">
                        <div class="input-container col-lg-8 col-sm-4 col-form-label">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus/>
                            <span class="label_fa"><i class="fas fa-envelope"></i></span>
                            <label for="email">
                                {{ __('Adresse e-mail') }}
                            </label>
                            <div class="bar"></div>
                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                              @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                          <div class="input-container col-lg-8 col-sm-2 col-form-label">
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            <span class="label_fa"><i class="fas fa-lock"></i></span>
                            <label for="password">{{ __('Mot de passe') }}</label>
                                <div class="bar"></div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                        </div>
                      </div>
                        <div class="form-group row">
                            <div class="col-md-12 col-form-label text-center">
                                <div class="custom-control custom-checkbox ">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    @if (Route::has('password.request'))
                                    <a class="txt1" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="button_login">
                                    <span><i class="fas fa-sign-in-alt"></i></span>{{ __('Se connecter') }}
                                </button>
                                <hr>
                            </div>
                            <div class="col-md-10 text-center">
                                <a href="#">
                                    <button type="btn" class="button_login_facebook">
                                        <span><i class="fab fa-facebook-square"></i></span>{{ __('Créer votre compte avec Facebook') }}
                                    </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header ">{{ __('Créer un compte') }}</div>

                <div class="card-body">
                    <div class="cont_form">
                    <div class="description ">
                      <div class="col-md-12">  Créez votre compte  !<br/>
                        Vous pouvez vous inscrire soit en utilisant votre adresse e-mail,
                        soit via votre compte Facebook.</div>
                    </div>
                    </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <a href="{{route('register')}}">
                                    <button type="btn" class="button_login">
                                        <span><i class="fas fa-sign-in-alt"></i></span>{{ __('Créer votre compte') }}
                                    </button>
                                    <hr>
                                </a>
                            </div>
                            <div class="col-md-10 text-md-left">
                                <a href="#">
                                    <button type="btn" class="button_login_facebook">
                                        <span><i class="fab fa-facebook-square"></i></span>{{ __('Créer votre compte avec Facebook') }}
                                    </button>
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

