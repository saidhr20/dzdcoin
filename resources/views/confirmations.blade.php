<!DOCTYPE html>
<html lang="en">
<head>
    <title>confirmation</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="OneTech shop project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href={!! asset("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css") !!} integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"  crossorigin="anonymous">
    <link href={!! asset("plugins/fontawesome-free-5.0.1/css/fontawesome-all.css") !!} rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.carousel.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/owl.theme.default.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("plugins/OwlCarousel2-2.2.1/animate.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_styles.css") !!} >
    <link rel="stylesheet" type="text/css" href={!! asset("styles/product_responsive.css") !!} >
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href={!! asset("styles/header.css") !!}>
</head>

<body>

<div class="super_container">
    @include('header')
    <div class="container">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h2>Confirmation Page</h2>
        </div>
        <div class="row">
            <div class="col-md-5 order-md-3 mb-5">
                <div class="container-info">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="deals_item_name active">Votre commande</span>
                        <span class="badge badge-danger badge-pill">{{Cart::instance('EURO_CART')->count()}}</span>
                    </h4>
                    <div class="deals_item">
                        <ul class="list-group mb-3">
                            @foreach(Cart::instance('EURO_CART')->content() as $item)
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <div class="char_icon">
                                            <span class="deals_timer_title">{{$item->name}} :</span>
                                        </div>
                                        <span class="badge badge-secondary"><small>{{$item->qty." ".$item->model->etq}}</small></span>
                                    </div>
                                    <span class="bestsellers_price discount">{{$item->subtotal(0)." ".$item->model->market}}</span>
                                </li>
                            @endforeach
                            <li class="list-group-item d-flex justify-content-between ">
                                <span>Total {{$item->model->etq}}</span>
                                <strong>{{Cart::instance('EURO_CART')->subtotal(0)." ".$item->model->market}}</strong>
                            </li>
                        </ul>
                        <div class="container-perso">
                            <div class="d-flex justify-content-between">
                                <div class="char_icon">Versez à : </div>
                                <div class="discount"> :ادفع الى</div>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between">
                                <div class="char_icon">Paysera details</div>
                                <div class="discount">+213697883500</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="char_icon">IBAN </div>
                                <div class="discount"> LT163500010002603904</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="char_icon">Paypal</div>
                                <div class="discount"> saidhr20@gmail.com </div>
                            </div>
                        </div>

                        <!--div class="col-sm">
                            <div>:  بريدي موب </div>
                            <div> 00000000000000 </div>
                        </div>

                        <div class="col-sm">
                            <div>: يد بيد </div>
                            <div> ملاحظة : البليدة فقط </div>
                        </div-->
                    </div>
                </div>







            </div>
            <div class="col-md-1 order-md-2 mb-1"></div>
            <div class="col-md-6 order-md-1 mb-6">
                <div class="container-info">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="deals_item_name active">Votre information</span>
                    </h4>
                    <div class="deals_item">
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-user"></i>
                                        <span class="deals_timer_title">Name & last name :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$user->first_name." ".$user->last_name}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-envelope"></i>
                                        <span class="deals_timer_title">Adresse Email :</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$user->email}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fas fa-phone-square"></i>
                                        <span class="deals_timer_title">Phone number:</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$user->phone}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="char_icon">
                                        <i class="fa fa-id-card" aria-hidden="true"></i>
                                        <span class="deals_timer_title">Payment method</span>
                                    </div>

                                </div>
                                <span class="bestsellers_price discount">{{$user->paymentMethod}}</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between ">
                                <span></span>
                                <button class="btn btn-light">
                                 <a href="{{route('checkout.create')}}">Edit your information</a>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <button class="btn btn-primary btn-lg btn-block"> Confirm the process </button>
                </div>
            </div>
        </div>




    </div>


</div>


<div class="py-5 text-center">
    @include('footer')

</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js"></script>
</body>
</html>


