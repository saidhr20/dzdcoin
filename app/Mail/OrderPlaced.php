<?php

namespace App\Mail;

use App\order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;
    public  $order;

    /**
     * OrderPlaced constructor.
     * @param order $order
     */
    public function __construct(order $order)
    {
        $this->order=$order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->order->email, $this->order->first_name)
            ->bcc('another@another.com')
            ->subject('Votre commande dzdcash '.$this->order->id. ' a été reçue passez a l\'étape suivante. Merci!')
            ->markdown('emails.orders.placed');
    }
}
