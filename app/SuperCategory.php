<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperCategory extends Model
{
    protected  $table='super_categories';
    public function category()
    {
        return  $this->belongsToMany('App\category');
    }
}
