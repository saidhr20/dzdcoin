<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable=[
        'user_id',
        'email',
        'first_name',
        'last_name',
        'address',
        'wilaya',
        'postal',
        'phone',
        'paymentMethod',
        'additional',
        'subtotal',
        'shipped',
        'error',
    ];
    public function user(){
       return $this->belongsTo('App\User');
    }
    public function product(){
        return $this->belongsToMany('App\product')->withPivot('quantity');
    }

}
