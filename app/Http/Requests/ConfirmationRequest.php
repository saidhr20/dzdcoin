<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $emailvalidation=auth()->user() ?'required|email':'required|email|unique:users';
        $phoneValidation=array('phone' => 'required', 'regex:/(00213|\+213|0)(5|6|7)[0-9]{8}/');

        return [
            'email' => $emailvalidation,
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required',
            'wilaya' => 'required',
            'postal' => 'required|numeric',
            'phone' => $phoneValidation,
            'paymentMethod' => 'required|not_in',
        ];
    }
    public function messages()
    {
        return [
            'email.unique'=>'Vous avez déjà un compte avec cette adresse email, <a href="/login" class="btn-link">connectez-vous</a> pour continuer votre achat',

        ];
    }

}
