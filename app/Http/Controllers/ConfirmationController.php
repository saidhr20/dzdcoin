<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmationRequest;
use App\Mail\OrderPlaced;
use App\order;
use App\orderProduct;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!session()->has('success_message') )
        {
           return redirect('/');
        }
    else{
        $order = order::get()->last();
        $cart = $order->product;
    return view('confirmation')->with([
        'order'=>$order,
        'cart'=>$cart
    ]);
    }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfirmationRequest $request)
    {

        $user =new User();
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->address=$request->address;
        $user->wilaya=$request->wilaya;
        $user->postal=$request->postal;
        $user->phone=$request->phone;
        $user->paymentMethod=$request->paymentMethod;
        $user->additional=$request->additional;
        $subtotal=Cart::instance('DZD_CART')->subtotal(0);
        $this->addToOrdersTables($user,$subtotal,null);


        return redirect()->route('confirmation.index')->with([
            'success_message'=>'nous avons bien reçu votre commande  , veuillez suivre les instructions pour faire votre paiement',

        ]);


    }

    public function store2(ConfirmationRequest $request)
    {

        $user =new User();
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->address=$request->address;
        $user->wilaya=$request->wilaya;
        $user->postal=$request->postal;
        $user->phone=$request->phone;
        $user->paymentMethod=$request->paymentMethod;
        $user->additional=$request->additional;
        $subtotal=Cart::instance('EURO_CART')->subtotal();
        $this->addToOrdersTables($user,$subtotal,null);

        return redirect()->route('confirmation.index')->with([
            'success_message'=>'nous avons bien reçu votre commande  , veuillez suivre les instructions pour faire votre paiement',

        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    protected function addToOrdersTables($user, $subtotal,$error)
    {
        // Insert into orders table

        if(Cart::count()>0 && !empty($user)) {
        $order = order::create([

            'user_id' => auth()->user() ? auth()->user()->id : null,
            'email' => filter_var(strip_tags(htmlentities($user->email)),FILTER_SANITIZE_STRING),
            'first_name' => filter_var(strip_tags(htmlentities($user->first_name)),FILTER_SANITIZE_STRING),
            'last_name' => filter_var(strip_tags(htmlentities($user->last_name)),FILTER_SANITIZE_STRING),
            'address' => filter_var(strip_tags(htmlentities($user->address)),FILTER_SANITIZE_STRING),
            'wilaya' => filter_var(strip_tags(htmlentities($user->wilaya)),FILTER_SANITIZE_STRING),
            'postal' => filter_var(strip_tags(htmlentities($user->postal)),FILTER_SANITIZE_STRING),
            'phone' => filter_var(strip_tags(htmlentities($user->phone)),FILTER_SANITIZE_STRING),
            'paymentMethod' => filter_var(strip_tags(htmlentities($user->paymentMethod)),FILTER_SANITIZE_STRING),
            'additional' =>filter_var(strip_tags(htmlentities($user->additional)),FILTER_SANITIZE_STRING),
            'subtotal' => $subtotal,
            'error' => $error,
        ]);

        // Insert into order_product table
        foreach (Cart::content() as $item) {
            orderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
            ]);
        }

        Cart::destroy();
            Mail::send(new OrderPlaced($order));
        }
        else
        {
            return redirect()->route('confirmation.index')->with([
                'error_message'=>'donnée invalide , veuillez réessayer ultérieurement ',

            ]);
        }

    }

}
