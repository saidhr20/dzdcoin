<?php

namespace App\Http\Controllers;

use App\category;
use App\product;
use App\SuperCategory;
use Illuminate\Http\Request;

class shopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->category){
        $products=product::with('categories')->whereHas('categories', function ($query){
            $query->where('slugs',request()->category);
        })->get();
            $superCategories =SuperCategory::all();
            $categories = category::all();
            $categoryName = optional($categories->where('slugs',request()->category)->first())->name;
        }
       else{
            $superCategories =SuperCategory::all();
            $products = product::get();
            $categories = category::all();
            $categoryName='TOUTES LES CATÉGORIES';
       }
        $recentViewed= product::take(10)->get();
        return view('shop')->with([
            'product'=>$products,
            'recentViewed'=>$recentViewed,
            'categories'=>$categories,
            'categoryName'=> $categoryName,
            'superCategories'=> $superCategories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product= product::where('slug',$slug)->firstOrFail();
        $recentViewed= product::where('slug',"!=",$slug)->inRandomOrder()->take(10)->get();
        return view('product')->with([
            'product'=>$product,
            'recentViewed'=>$recentViewed
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
