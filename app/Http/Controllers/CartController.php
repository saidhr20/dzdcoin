<?php

namespace App\Http\Controllers;

use App\product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\category;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('carte');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(product $product, Request $request)
    {
        $duplicates_DZD=Cart::instance('DZD_CART')->search(function ($cartItem,$rowId)use ($product){return $cartItem->id===$product->id;});
        if ($duplicates_DZD->isNotEmpty()){
            return redirect()->route('cart.index')->with('info_message','l\'article est déja exister sur votre panier');
        }
        //check quantity
        $qtyValide= 'numeric|integer|between:'.$product->min_buy.','.$product->max_buy;
        $this->validate($request,[
                'quantity' => $qtyValide,
        ]);
        session()->flash('error');
        if(empty($request->quantity))
            {
                $request->quantity=$product->min_buy;
            }

            Cart::instance('DZD_CART')->add($product->id, $product->name, $request->quantity, $product->price)
                ->associate('App\product');
            return redirect()->route('cart.index')
                ->with('success_message', 'l\'article a été ajouté à votre panier');

         // add to euro cart

    }



    /**
     * Display the specified resource.
     *
     *@param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator= Validator::make($request->all(),[
            'quantity'=>'required|numeric|integer|between:'.$request->min.','.$request->max
        ]);
        if($validator->fails()){
            session()->flash('errors_qty','La quantité de "'.$request->name.'" doit être comprise entre '.$request->min.' et '.$request->max);
            return response()->json(['succes' => false],400);
        }
        Cart::instance('DZD_CART')->update($id,$request->quantity);
        session()->flash('success_message','Quantité mise à jours');
        return response()->json(['succes' => true]);
    }



    // cart EURO update
         public function update2(Request $request, $id)
        {
         $validator= Validator::make($request->all(),[
             'quantity'=>'required|numeric|integer|between:'.$request->min.','.$request->max
         ]);
        if($validator->fails()){
            session()->flash('errors_qty','La quantité de "'.$request->name.'" doit être comprise entre '.$request->min.' et '.$request->max);
            return response()->json(['succes' => false],400);
            }
            Cart::instance('EURO_CART')->update($id,$request->quantity);
            session()->flash('success_message','Quantité mise à jours');
            return response()->json(['succes' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Cart::instance('DZD_CART')->remove($id);
        return back()->with('success_message','L\'article a bien été retiré de votre panier');

    }
    public function destroy2($id)
    {

        Cart::instance('EURO_CART')->remove($id);
        return back()->with('success_message','L\'article a bien été retiré de votre panier');

    }
}
