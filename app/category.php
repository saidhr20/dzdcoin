<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected  $table='category';
    public function SupCategory()
    {
        return  $this->belongsToMany('App\SuperCategory');
    }
    public function product()
    {
       return  $this->belongsToMany('App\product');
    }
}
