<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorySubCategory extends Model
{
    protected $table="category_super_category";
    protected $fillable =['super_category_id','category_id'];
}
