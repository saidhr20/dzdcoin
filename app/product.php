<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = "product";

    public function categories()
    {
        return  $this->belongsToMany('App\category');
    }
}
