<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::view('/','main' );
Route::get('/','mainPageController@index')->name('main');

Route::get('/shop','shopController@index' )->name('shop.index');
Route::get('/shop/{product}','shopController@show' )->name('shop.show');

Route::get('/cart','CartController@index' )->name('cart.index');
Route::post('/cart/{product}','CartController@store' )->name('cart.store');
Route::delete('/cart/{product}','CartController@destroy' )->name('cart.destroy');
Route::patch('/cart/{product}','CartController@update' )->name('cart.update');


Route::get('/carte','CartController@create' )->name('carte.create');
Route::delete('/carte/{product}','CartController@destroy2' )->name('carte.destroy2');
Route::patch('/carte/{product}','CartController@update2' )->name('carte.update2');

Route::get('/contact','contactController@index' )->name('contact.index');


Route::get('/checkout','checkoutController@index' )->name('checkout.index');//cart dzd form show
Route::get('/check-out','checkoutController@create' )->name('checkout.create');// cart euro index from show


Route::get('/confirmation','ConfirmationController@index' )->name('confirmation.index');//page confrimation
Route::post('/confirmation','ConfirmationController@store' )->name('confirmation.store');//database order confrimation dzd
Route::post('/confirmations','ConfirmationController@store2' )->name('confirmation.store2');//database order confrimation euro

Route::get('/guide',function () {return view('guide');})->name('guide');
Route::get('/faq',function () {return view('faq');})->name('faq');

Auth::routes();
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Route::view('/welcome','welcome');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
