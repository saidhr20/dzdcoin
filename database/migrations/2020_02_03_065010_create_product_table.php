<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->Increments('id')->unique();
            $table->string('name');
            $table->string('slug');
            $table->float('price');
            $table->float('last_price')->nullable();
            $table->integer('stock');
            $table->integer('sold');
            $table->integer('max_buy');
            $table->Integer('min_buy');
            $table->string('etq')->nullable();
            $table->boolean('feature')->default(false);
            $table->text('description')->nullable();
            $table->date('updated_at')->nullable();
            $table->date('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
