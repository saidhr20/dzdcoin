<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->bigIncrements('id', true);
            $table->bigInteger('user_id')->unsigned()->nullable()->index();

            $table->string('email');
             $table->string('first_name');
             $table->string('last_name');
             $table->text('address');
             $table->string('wilaya');
             $table->string('postal');
             $table->string('phone');
             $table->string('paymentMethod');
             $table->text('additional');
             $table->string('subtotal');
             $table->boolean('shipped')->default(false);
             $table->string('error')->nullable();
             $table->timestamps();
        });
        Schema::table('orders', function($table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
