<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->bigInteger('user_id')->unsigned()->nullable()->index();
            $table->integer('product_id')->unsigned()->nullable();

            $table->string('name');
            $table->string('email');
            $table->string('rating');
            $table->text('reviewing');
            $table->text('image')->nullable();
            $table->boolean('approved')->default(false);
            $table->timestamps();
        });
        Schema::table('reviews', function($table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
        });
        Schema::table('reviews', function($table) {
            $table->foreign('product_id')->references('id')
                ->on('product')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
