<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->bigIncrements('id',true)->index();
            $table->bigInteger('order_id')->unsigned()->nullable()->index();


            $table->integer('product_id')->unsigned()->nullable();


            $table->integer('quantity')->unsigned();
            $table->timestamps();
        });
        Schema::table('order_product', function($table) {

            $table->foreign('order_id')->references('id')
                ->on('orders')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('product_id')->references('id')
                ->on('product')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
