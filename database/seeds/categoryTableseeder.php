<?php

use App\category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class categoryTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now=Carbon::now()->toDateTimeString();

        category::insert([
            ['name'=>'Solde','slugs'=>'solde','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Abonnement','slugs'=>'abonnement','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Rechagement','slugs'=>'rechargement','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Smartphones & Tablets','slugs'=>'smartphone','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Tv & Serveurs IpTv','slugs'=>'serveurs-tv','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Videos games & credits','slugs'=>'videos-games','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Services Electronique','slugs'=>'services-electronique','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Accessories','slugs'=>'accessories','created_at'=>$now,'updated_at'=>$now]

        ]);
    }
}
