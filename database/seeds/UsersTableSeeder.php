<?php

use App\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $role = Role::where('name', 'admin')->firstOrFail();

            User::create([
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt(config('voyager.adminPassword')),
                'remember_token' => str_random(60),
                'role_id'        => $role->id,
            ]);
        }
    }
}
